//
//  AboutNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 11/01/24.
//

import Foundation
import AsyncDisplayKit

final class AboutNode: ASDisplayNode {
	
	var onDone: (() -> Void)?
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var sideInset: CGFloat {
		return isSmallDevice ? 20 : 30
	}
	
	private var inset: UIEdgeInsets {
		return UIEdgeInsets(top: ScreenInfo.safeAreaInsets.top + 30 + 24,
							left: sideInset,
							bottom: ScreenInfo.safeAreaInsets.bottom + 20,
							right: sideInset)
	}
	
	private let imageNode = ASImageNode()
	private let copyrightNode = ASTextNode()
	
	private lazy var collectionNode: ASCollectionNode = {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .vertical
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		
		let collectionNode = ASCollectionNode(collectionViewLayout: layout)
		
		return collectionNode
	}()
	
	override init() {
		super.init()
		automaticallyManagesSubnodes = true
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		return ASInsetLayoutSpec(insets: inset, child: collectionNode)
	}
	
	private func configureNodes() {
		configureImageNode()
		configureCollectionNode()
		configureTextNode()
	}
	
	private func configureImageNode() {
		imageNode.image = UIImage(named: "logo")
		imageNode.style.preferredSize = CGSize(width: 160, height: 180)
		imageNode.contentMode = .scaleAspectFit
	}
	
	private func configureCollectionNode() {
		collectionNode.dataSource = self
		collectionNode.delegate = self
		collectionNode.backgroundColor = .clear
		collectionNode.view.contentInsetAdjustmentBehavior = .never
		collectionNode.showsVerticalScrollIndicator = false
	}
	
	private func configureTextNode() {
		let copyrightStyle = FontStyle(font: .regular, color: .white, size: isSmallDevice ?  12 : 14, alignment: .center)
		copyrightNode.attributedText = copyrightStyle.getAttributedString(from: "Copyright © 2023-2024. All rights reserved.")
	}
}

extension AboutNode: ASCollectionDataSource, ASCollectionDelegate {
	func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
		return 3
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
		switch section {
		case 1:
			return 6
		default:
			return 1
		}
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
		return { [weak self] in
			
			switch indexPath.section {
			case 0:
				return self?.getWrapperCellNode(for: self?.imageNode) ?? ASCellNode()
			case 1:
				return self?.getContentCellNode(for: indexPath.row) ?? ASCellNode()
			case 2:
				return self?.getWrapperCellNode(for: self?.copyrightNode) ?? ASCellNode()
			default:
				return ASCellNode()
			}
		}
	}
	
	private func getWrapperCellNode(for node: ASControlNode?) -> ASCellNode {
		guard let node else {
			return ASCellNode()
		}
		
		let cellNode = ASCellNode()
		cellNode.automaticallyManagesSubnodes = true
		cellNode.layoutSpecBlock = { _, _ in
			let centerSpec = ASCenterLayoutSpec(child: node)
			return ASInsetLayoutSpec(insets: .zero, child: centerSpec)
		}
		
		cellNode.style.minSize = CGSize(width: ScreenInfo.width - 2*sideInset, height: 0)
		
		return cellNode
	}
	
	private func getContentCellNode(for row: Int) -> ASCellNode {
		switch row {
		case 0:
			return AboutCellNode(title: "Welcome to Focus Safari!",
								 desc: "Focus Safari is a unique productivity app that transforms your focus and concentration efforts into an exciting and rewarding experience. Designed with a creative blend of focus management tools and engaging gameplay, our app is here to make productivity both fun and educational.")
		case 1:
			return FeatureCollectionNode()
		case 2:
			return AboutCellNode(title: "Why Focus Safari? 🎯",
								 desc: "We believe productivity can be more than just ticking off tasks. It's about learning, growing, and enjoying the process. Focus Safari is designed to make each focus session a journey towards better productivity, with a touch of fun and learning.")
		case 3:
			return AboutCellNode(title: "Developed with Passion 💖",
								 desc: "Our small but dedicated team from Indonesia has poured their creativity and expertise into every aspect of Focus Safari, ensuring a seamless and enjoyable user experience.")
		case 4:
			return AboutCellNode(title: "Privacy and Security 🔒",
								 desc: "Your privacy matters to us. We've implemented robust security measures to protect your data and ensure a safe and secure app environment.")
		case 5:
			return AboutCellNode(title: "Your Feedback Makes Us Better! 🌟",
								 desc: "We thrive on your feedback! Every review you leave on the App Store is more than just words—it's valuable insight that helps us improve and grow. So, take a moment to leave us a review. Your thoughts, suggestions, and experiences are the guiding stars for our continuous improvement and innovation.")
		default:
			return ASCellNode()
		}
	}
}
