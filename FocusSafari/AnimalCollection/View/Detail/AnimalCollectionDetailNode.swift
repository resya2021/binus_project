//
//  AnimalCollectionDetailNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 12/01/24.
//

import Foundation
import AsyncDisplayKit

final class AnimalCollectionDetailNode: ASDisplayNode {
	
	var onDone: (() -> Void)?
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var sideInset: CGFloat {
		return isSmallDevice ? 16 : 20
	}
	
	private var inset: UIEdgeInsets {
		return UIEdgeInsets(top: ScreenInfo.safeAreaInsets.top + 30 + 24,
							left: sideInset,
							bottom: ScreenInfo.safeAreaInsets.bottom + 20,
							right: sideInset)
	}
	
	private let imageNode = ASImageNode()
	private let bgNode = ASDisplayNode()
	
	private lazy var collectionNode: ASCollectionNode = {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .vertical
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		
		let collectionNode = ASCollectionNode(collectionViewLayout: layout)
		
		return collectionNode
	}()
	
	private let viewModel: AnimalModel
	
	init(viewModel: AnimalModel) {
		self.viewModel = viewModel
		super.init()
		automaticallyManagesSubnodes = true
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		return ASInsetLayoutSpec(insets: inset, child: collectionNode)
	}
	
	func setGradient() {
		bgNode.gradient(from: .white, to: .gradientGray)
	}
	
	private func configureNodes() {
		configureImageNode()
		configureCollectionNode()
	}
	
	private func configureImageNode() {
		imageNode.image = UIImage(named: viewModel.assetName)
		imageNode.style.preferredSize = CGSize(width: ScreenInfo.width - 2*sideInset,
											   height: ScreenInfo.width - 2*sideInset)
		imageNode.contentMode = .scaleAspectFit
	}
	
	private func configureCollectionNode() {
		collectionNode.dataSource = self
		collectionNode.delegate = self
		collectionNode.backgroundColor = .clear
		collectionNode.view.contentInsetAdjustmentBehavior = .never
		collectionNode.showsVerticalScrollIndicator = false
	}
	
	private func getCellViewModel(for row: Int) -> AnimalCollectionDetailCellViewModel? {
		let info = viewModel.extendedInfo?[String(row)]
		
		if let singleInfo = info as? [String: String], let cellModel = singleInfo.first {
			return AnimalCollectionDetailCellViewModel(title: cellModel.key, desc: cellModel.value)
		} else if let multiInfo = info as? [String: [String: String]], let cellModel = multiInfo.first {
			return AnimalCollectionDetailCellViewModel(title: cellModel.key, multiDesc: cellModel.value)
		}
		
		return nil
	}
}

extension AnimalCollectionDetailNode: ASCollectionDataSource, ASCollectionDelegate {
	func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
		return 2
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
		if section == 0 {
			return 1
		} else {
			return viewModel.extendedInfo?.count ?? 0
		}
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
		return { [weak self] in
			
			switch indexPath.section {
			case 0:
				return self?.getWrapperCellNode(for: self?.imageNode) ?? ASCellNode()
			default:
				guard let cellViewModel = self?.getCellViewModel(for: indexPath.row) else {
					return ASCellNode()
				}
				
				return AnimalCollectionDetailCellNode(viewModel: cellViewModel)
			}
		}
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, willDisplayItemWith node: ASCellNode) {
		if node is AnimalCollectionDetailCellNode {
			return
		}
		
		setGradient()
	}
	
	private func getWrapperCellNode(for node: ASControlNode?) -> ASCellNode {
		guard let node else {
			return ASCellNode()
		}
		
		let cellNode = ASCellNode()
		cellNode.automaticallyManagesSubnodes = true
		cellNode.layoutSpecBlock = { [weak self] _, _ in
			guard let self else {
				return ASLayoutSpec()
			}
			
			self.bgNode.cornerRadius = 5
			
			let bgSpec = ASBackgroundLayoutSpec(child: node, background: bgNode)
			let centerSpec = ASCenterLayoutSpec(child: bgSpec)
			return ASInsetLayoutSpec(insets: .zero, child: centerSpec)
		}
		
		cellNode.style.minSize = CGSize(width: ScreenInfo.width - 2*sideInset, height: 0)
		
		return cellNode
	}
}
