//
//  HomeViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import Foundation
import AsyncDisplayKit

final class HomeViewController: ASDKViewController<ASDisplayNode> {
	
	private var mainNode: HomeNode?
	private var viewModel: HomeViewModel?
	
	private var audioPlayer:AVAudioPlayer?
	
	init(viewModel: HomeViewModel) {
		let mainNode = HomeNode(viewModel: viewModel)
		self.mainNode = mainNode
		self.viewModel = viewModel
		
		super.init(node: mainNode)
		configureMainNode()
		configureSound()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureNavigationBar()
	}
	
	private func configureNavigationBar() {
		
		let titleLabel = UILabel()
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: 20, alignment: .center)
		titleLabel.attributedText = titleStyle.getAttributedString(from: "Focus Safari")
		
		navigationItem.titleView = titleLabel
		
		let menuButton = UIButton(type: .custom)
		menuButton.setImage(UIImage(named: "menu_icon"), for: .normal)
		menuButton.addTarget(self, action: #selector(menuButtonPressed), for: .touchUpInside)
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
		
		let soundButton = UIButton(type: .custom)
		soundButton.isSelected = true
		soundButton.setImage(UIImage(named: "icon_sound"), for: .selected)
		soundButton.setImage(UIImage(named: "icon_mute"), for: .normal)
		soundButton.addTarget(self, action: #selector(soundButtonPressed), for: .touchUpInside)
		soundButton.isHidden = true
		
		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: soundButton)
	}
	
	private func configureSound() {
		let path = Bundle.main.path(forResource: "forest.mp3", ofType:nil)!
		let url = URL(fileURLWithPath: path)
		
		do {
			let player = try AVAudioPlayer(contentsOf: url)
			self.audioPlayer = player
			self.audioPlayer?.numberOfLoops = -1
			self.audioPlayer?.prepareToPlay()
		} catch {
			print("Error preparing audio player: \(error)")
		}
	}
	
	private func configureMainNode() {
		mainNode?.onChangeDuration = { [weak self] in
			let minutePicker: UIDatePicker = UIDatePicker()
			minutePicker.datePickerMode = .countDownTimer
			minutePicker.countDownDuration = 900
			minutePicker.minuteInterval = 5
			minutePicker.frame = CGRectMake(-100, 15, 200, 200)
			
			let view = UIView()
			view.frame = CGRectMake(100, 15, 80, 200)
			view.addSubview(minutePicker)
			view.clipsToBounds = true
			
			let alertController = UIAlertController(title: "Duration \n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
			alertController.view.addSubview(view)
			
			let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
				self?.mainNode?.timerRemaining = Int(minutePicker.countDownDuration)
			}
			
			alertController.addAction(confirmAction)
			self?.present(alertController, animated: true, completion:{})
		}
		
		mainNode?.onGetReward = { [weak self] minutes, isTimerInterrupted in
			
			self?.audioPlayer?.pause()
			self?.audioPlayer?.currentTime = 0
			
			guard minutes > 0 else {
				return
			}
			
			let viewModel = RewardViewModel(duration: minutes, isInterrupted: isTimerInterrupted)
			let viewController = RewardViewController(viewModel: viewModel)
			viewController.modalPresentationStyle = .popover
			viewController.preferredContentSize = .init(width: ScreenInfo.width, height: ScreenInfo.height)
			viewController.popoverPresentationController?.sourceView = RewardNode(viewModel: viewModel).view
			viewController.popoverPresentationController?.popoverLayoutMargins = UIEdgeInsets(top: -ScreenInfo.safeAreaInsets.top, left: .zero, bottom: -16, right: .zero)
			viewController.popoverPresentationController?.popoverBackgroundViewClass = PopoverBackgroundView.self
			viewController.popoverPresentationController?.permittedArrowDirections = []
			viewController.popoverPresentationController?.delegate = self
			
			self?.present(viewController, animated: true)
		}
		
		mainNode?.onShowSoundButton = { [weak self] shouldShow in
			self?.navigationItem.rightBarButtonItem?.customView?.isHidden = !shouldShow
			self?.navigationItem.leftBarButtonItem?.customView?.isHidden = shouldShow
			
			if shouldShow {
				DispatchQueue.main.async {
					self?.audioPlayer?.play()
				}
			}
		}
		
		mainNode?.onShowWelcome = { [weak self] in
			let viewController = WelcomeViewController()
			self?.present(viewController, animated: true)
		}
	}
	
	@objc
	private func menuButtonPressed() {
		let viewController = SidePopUpViewController()
		viewController.modalPresentationStyle = .popover
		viewController.preferredContentSize = .init(width: 236, height: 250)
		viewController.popoverPresentationController?.sourceView = SidePopUpNode().view
		viewController.popoverPresentationController?.popoverLayoutMargins = UIEdgeInsets(top: 8, left: -10, bottom: 0, right: 0)
		viewController.popoverPresentationController?.permittedArrowDirections = []
		viewController.popoverPresentationController?.delegate = self
		
		viewController.onNavigate = { [weak self] event in
			var destinationVC = UIViewController()
			
			switch event {
			case .home:
				return
			case .collection:
				let collectionVC = AnimalCollectionViewController(viewModel: AnimalCollectionViewModel())
				
				collectionVC.onPush = { [weak self] vc in
					self?.navigationController?.pushViewController(vc, animated: true)
				}
				
				destinationVC = collectionVC
			case .shop:
				destinationVC = ShopViewController(viewModel: ShopViewModel())
			case .account:
				let accountVC = AccountViewController(viewModel: AccountViewModel())
				
				accountVC.onPush = { [weak self] vc in
					self?.navigationController?.pushViewController(vc, animated: true)
				}
				
				destinationVC = accountVC
				
			case .about:
				destinationVC = AboutViewController()
			}
			
			self?.navigationController?.pushViewController(destinationVC, animated: true)
		}
		
		present(viewController, animated: true)
	}
	
	@objc
	private func soundButtonPressed(sender: UIButton) {
		sender.isSelected.toggle()
		
		if sender.isSelected {
			audioPlayer?.play()
		} else {
			audioPlayer?.pause()
		}
	}
}

extension HomeViewController: UIPopoverPresentationControllerDelegate {
	func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
		return .none
	}
	
	func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
		return true
	}
}
