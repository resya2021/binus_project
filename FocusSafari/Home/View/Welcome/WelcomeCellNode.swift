//
//  WelcomeCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 11/01/24.
//

import Foundation
import AsyncDisplayKit

final class WelcomeCellNode: ASCellNode {
	
	private let titleNode = ASTextNode()
	private let descriptionNode = ASTextNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var cellInset: CGFloat {
		return isSmallDevice ? 20 : 30
	}
	
	private let title: String
	private let desc: String
	
	init(title: String, desc: String) {
		self.title = title
		self.desc = desc
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let textStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 12,
			justifyContent: .start,
			alignItems: .start,
			children: [
				titleNode,
				descriptionNode
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0), child: textStack)
	}
	
	private func configureNodes() {
		let titleStyle = FontStyle(font: .bold, color: .deepTeal, size: isSmallDevice ?  16 : 18, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: self.title)
		
		let descriptionStyle = FontStyle(font: .regular, color: .gray61, size: isSmallDevice ?  14 : 16, alignment: .left)
		descriptionNode.attributedText = descriptionStyle.getAttributedString(from: self.desc)
		descriptionNode.style.minSize = CGSize(width: ScreenInfo.width - 2*cellInset, height: 0)
	}
}

