//
//  ShopCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation
import AsyncDisplayKit

final class ShopCellNode: ASCellNode {
	
	var onHatch: ((EggRarity) -> Void)?
	
	private let imageNode = ASImageNode()
	private let titleNode = ASTextNode()
	private let descriptionNode = ASTextNode()
	private let gemIconNode = ASImageNode()
	private let priceNode = ASTextNode()
	private let buttonNode = ASButtonNode()
	private let bgNode = ASDisplayNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var cellInset: CGFloat {
		return isSmallDevice ?  16 : 20
	}
	
	private var stackSpacing: CGFloat {
		return isSmallDevice ?  6 : 8
	}
	
	private let item: ShopItemModel
	
	init(item: ShopItemModel) {
		self.item = item
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let textStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: stackSpacing,
			justifyContent: .start,
			alignItems: .start,
			children: [
				titleNode,
				descriptionNode
			])
		
		let priceStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 4,
			justifyContent: .end,
			alignItems: .center,
			children: [
				gemIconNode,
				priceNode
			])
		
		let buttonStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: stackSpacing,
			justifyContent: .end,
			alignItems: .end,
			children: [
				priceStack,
				buttonNode
			])
		
		let rightStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 2,
			justifyContent: .spaceBetween,
			alignItems: .end,
			children: [
				textStack,
				buttonStack
			])
		
		rightStack.style.minSize = CGSize(width: 0, height: 150)
		
		let mainStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: stackSpacing,
			justifyContent: .start,
			alignItems: .start,
			children: [
				imageNode,
				rightStack
			])
		
		let stackInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: cellInset,
																	left: cellInset,
																	bottom: cellInset,
																	right: cellInset),
											   child: mainStack)
		
		bgNode.cornerRadius = 10
		let bgSpec = ASBackgroundLayoutSpec(child: stackInsetSpec, background: bgNode)
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: cellInset/2,
													  left: cellInset,
													  bottom: cellInset/2,
													  right: cellInset),
								 child: bgSpec)
	}
	
	func setGradient() {
		bgNode.gradient(from: .white, to: .gradientGray)
		buttonNode.gradient(from: .lightBlue, to: .darkBlue, cornerRadius: 5)
	}
	
	private func configureNodes() {
		configureImageNodes()
		configureTextNodes()
		configureButtonNode()
	}
	
	private func configureImageNodes() {
		imageNode.image = UIImage(named: item.assetName)
		imageNode.style.preferredSize = CGSize(width: 113, height: 150)
		imageNode.contentMode = .scaleAspectFit
		
		gemIconNode.image = UIImage(named: "icon_shop_gem")
		gemIconNode.style.preferredSize = CGSize(width: 14, height: 12)
		gemIconNode.contentMode = .scaleAspectFit
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .semiBold, color: .deepTeal, size: isSmallDevice ?  16 : 18, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: item.name)
		
		let descriptionStyle = FontStyle(font: .regular, color: .gray61, size: isSmallDevice ?  10 : 12, alignment: .left)
		descriptionNode.attributedText = descriptionStyle.getAttributedString(from: item.description)
		descriptionNode.style.minSize = CGSize(width: ScreenInfo.width - 4*cellInset - stackSpacing - 113,
											   height: 0)
		descriptionNode.style.maxSize = CGSize(width: ScreenInfo.width - 4*cellInset - stackSpacing - 110,
											   height: 200)
		
		let priceStyle = FontStyle(font: .medium, color: .deepTeal, size: isSmallDevice ?  14 : 16, alignment: .right)
		priceNode.attributedText = priceStyle.getAttributedString(from: item.price.stringWithCommaSeparator)
	}
	
	private func configureButtonNode() {
		buttonNode.style.preferredSize = CGSize(width: isSmallDevice ?  80 : 100, height: isSmallDevice ?  32 : 40)
		buttonNode.cornerRadius = 5
		buttonNode.addTarget(self, action: #selector(hatchAnEgg), forControlEvents: .touchUpInside)
		
		let buttonStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  14 : 16, alignment: .center)
		buttonNode.setAttributedTitle(buttonStyle.getAttributedString(from: "Hatch!"), for: .normal)
	}
	
	@objc
	private func hatchAnEgg() {
		onHatch?(item.rarity)
	}
}
