//
//  AccountViewModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 10/01/24.
//

import Foundation

final class AccountViewModel {
	
	var userInfo: UserModel? {
		return UserService.shared.userInfo.value
	}
	
	var exp: Int {
		return userInfo?.exp ?? 0
	}
	
	var currentLevelExpRequirement: Int {
		return UserService.shared.currentLevelExpRequirement
	}
	
	var bio: String {
		return UserService.shared.userInfo.value?.bio ?? ""
	}
}
