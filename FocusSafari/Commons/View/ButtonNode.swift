//
//  ButtonNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 10/01/24.
//

import Foundation
import AsyncDisplayKit

final class ButtonNode: ASDisplayNode {
	
	var onDidTap: (() -> Void)?
	
	private let imageNode = ASImageNode()
	private let textNode = ASTextNode()
	
	private let iconName: String?
	private let text: String
	private let gradientColor1: UIColor
	private let gradientColor2: UIColor
	private let textColor: UIColor
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	init(iconName: String?,
		 text: String,
		 gradientFrom color1: UIColor = .lightBlue,
		 gradientTo color2: UIColor = .darkBlue,
		 textColor: UIColor = .white,
		 cornerRadius: CGFloat = 5
	) {
		self.iconName = iconName
		self.text = text
		self.gradientColor1 = color1
		self.gradientColor2 = color2
		self.textColor = textColor
		
		super.init()
		automaticallyManagesSubnodes = true
		self.cornerRadius = cornerRadius
		
		configureNodes()
	}
	
	override func didLoad() {
		super.didLoad()
		
		addTapGestureRecognizer()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		var children: [ASLayoutElement] = [textNode]
		
		if iconName != nil {
			children.insert(imageNode, at: 0)
		}
		
		let stack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 0,
			justifyContent: .spaceBetween,
			alignItems: .center,
			children: children)
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8), child: stack)
	}
	
	func setGradient() {
		gradient(from: gradientColor1, to: gradientColor2, cornerRadius: 5)
	}
	
	private func configureNodes() {
		if let iconName {
			imageNode.image = UIImage(named: iconName)
			imageNode.style.preferredSize = CGSize(width: 25, height: 25)
			imageNode.contentMode = .scaleAspectFit
		}
		
		let textStyle = FontStyle(font: .semiBold, color: textColor, size: isSmallDevice ?  14 : 16, alignment: .center)
		textNode.attributedText = textStyle.getAttributedString(from: text)
	}
	
	private func addTapGestureRecognizer() {
		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
		view.addGestureRecognizer(tapGesture)
		view.isUserInteractionEnabled = true
	}
	
	@objc
	private func onTap() {
		onDidTap?()
	}
}
