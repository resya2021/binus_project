//
//  RewardNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 09/01/24.
//

import Foundation
import AsyncDisplayKit

final class RewardNode: ASDisplayNode {
	
	var onClose: (() -> Void)?
	
	private let titleNode: ASTextNode = ASTextNode()
	private let logoNode: ASImageNode = ASImageNode()
	private let subtitleNode: ASTextNode = ASTextNode()
	private let durationNode: ASTextNode = ASTextNode()
	private let closeButton: ASButtonNode = ASButtonNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private let viewModel: RewardViewModel
	
	init(viewModel: RewardViewModel) {
		self.viewModel = viewModel
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func subnodeDisplayWillStart(_ subnode: ASDisplayNode) {
		closeButton.gradient(from: .lightYellow, to: .darkYellow)
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: isSmallDevice ?  12 : 16,
			justifyContent: .center,
			alignItems: .center,
			children: [
				titleNode,
				logoNode,
				subtitleNode,
				durationNode,
				closeButton
			])
		
		let insetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16),
										  child: stack)
		
		let bgNode = ASDisplayNode()
		bgNode.backgroundColor = .white
		bgNode.cornerRadius = 10
		
		let bgSpec = ASBackgroundLayoutSpec(child: insetSpec, background: bgNode)
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: .infinity, left: 16, bottom: .infinity, right: 16), child: bgSpec)
	}
	
	private func configureNodes() {
		configureTextNodes()
		configureImageNode()
		configureButtonNode()
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .semiBold, color: .teal, size: isSmallDevice ?  24 : 28, alignment: .center)
		titleNode.attributedText = titleStyle.getAttributedString(from: "Mission Completed!")
		
		let subtitleStyle = FontStyle(font: .semiBold, color: .teal, size: isSmallDevice ?  16 : 20, alignment: .center)
		subtitleNode.attributedText = subtitleStyle.getAttributedString(from: "You’ve Stayed Focused for")
		
		let durationStyle = FontStyle(font: .semiBold, color: .darkCyan, size: isSmallDevice ?  20 : 24, alignment: .center)
		durationNode.attributedText = durationStyle.getAttributedString(from: "\(viewModel.duration) Minutes")
	}
	
	private func configureImageNode() {
		let size: CGFloat = isSmallDevice ?  155 : 187
		logoNode.style.preferredSize = CGSize(width: size, height: size)
		logoNode.image = UIImage(named: "logo_round")
		logoNode.contentMode = .scaleAspectFit
	}
	
	private func configureButtonNode() {
		let buttonStyle = FontStyle(font: .semiBold, color: .deepTeal, size: isSmallDevice ?  16 : 20, alignment: .center)
		closeButton.setAttributedTitle(buttonStyle.getAttributedString(from: "CLOSE"), for: .normal)
		closeButton.style.preferredSize = CGSize(width: isSmallDevice ?  200 : 250, height: isSmallDevice ?  48 : 60)
		closeButton.cornerRadius = 10
		closeButton.addTarget(self, action: #selector(buttonPressed), forControlEvents: .touchUpInside)
	}
	
	@objc
	private func buttonPressed() {
		onClose?()
	}
}
