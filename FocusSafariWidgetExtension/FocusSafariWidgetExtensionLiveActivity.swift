//
//  FocusSafariWidgetExtensionLiveActivity.swift
//  FocusSafariWidgetExtension
//
//  Created by Anisa Budiarthati on 01/01/24.
//

import ActivityKit
import WidgetKit
import SwiftUI

@available(iOS 16.2, *)
struct FocusSafariWidgetExtensionLiveActivity: Widget {
    var body: some WidgetConfiguration {
        ActivityConfiguration(for: FocusSafariAttributes.self) { context in
            // Lock screen/banner UI goes here
			LiveActivityView(iconName: context.attributes.iconName,
							 endTime: context.state.endTime,
							 message: context.state.message)
			
        } dynamicIsland: { context in
            DynamicIsland {
                // Expanded UI goes here.  Compose the expanded UI through
                // various regions, like leading/trailing/center/bottom
                DynamicIslandExpandedRegion(.leading) {
                    Text("Leading")
                }
                DynamicIslandExpandedRegion(.trailing) {
                    Text("Trailing")
                }
                DynamicIslandExpandedRegion(.bottom) {
                    Text("Bottom")
                    // more content
                }
            } compactLeading: {
                Text("L")
            } compactTrailing: {
                Text("T")
            } minimal: {
                Text("🦁")
            }
            .widgetURL(URL(string: "fsafari://"))
            .keylineTint(Color.white)
        }
    }
}

@available(iOS 16.2, *)
struct FocusSafariWidgetExtensionLiveActivity_Previews: PreviewProvider {
    static let attributes = FocusSafariAttributes(iconName: "logo")
    static let contentState = FocusSafariAttributes.ContentState(endTime: Date(timeIntervalSinceNow: 10))
//	static let contentState = FocusSafariAttributes.ContentState(message: "Successfully focused!")

    static var previews: some View {
		LiveActivityView(iconName: attributes.iconName,
						 endTime: contentState.endTime,
						 message: contentState.message)
		.previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}
