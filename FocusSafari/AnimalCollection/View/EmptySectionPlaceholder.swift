//
//  EmptySectionPlaceholder.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation
import AsyncDisplayKit

final class EmptySectionPlaceholder: ASCellNode {
	
	private let imageNode = ASImageNode()
	private let textNode = ASTextNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private let rarity: EggRarity
	
	init(rarity: EggRarity) {
		self.rarity = rarity
		super.init()
		backgroundColor = .clear
		automaticallyManagesSubnodes = true
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 6,
			justifyContent: .center,
			alignItems: .center,
			children: [
				imageNode,
				textNode
			])
		
		let insetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 25, bottom: 10, right: 25),
										  child: stack)
		
		let node = ASDisplayNode()
		node.borderColor = UIColor.white.cgColor
		node.borderWidth = 2
		node.cornerRadius = 5
		
		let bgSpec = ASBackgroundLayoutSpec(child: insetSpec, background: node)
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0), child: bgSpec)
	}
	
	private func configureNodes() {
		configureImageNodes()
		configureTextNodes()
	}
	
	private func configureImageNodes() {
		imageNode.image = UIImage(named: "icon_magnifying_glass")
		imageNode.style.preferredSize = CGSize(width: 40, height: 40)
		imageNode.contentMode = .scaleAspectFit
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: 12, alignment: .center)
		textNode.attributedText = titleStyle.getAttributedString(from: "No \(rarity.string) Animal discovered.\nThey’re out there somewhere.")
	}
}
