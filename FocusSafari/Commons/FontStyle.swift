//
//  FontStyle.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import Foundation
import UIKit

final class FontStyle {
	
	enum Style: String {
		case regular = "Poppins-Regular"
		case regularItalic = "Poppins-Italic"
		case medium = "Poppins-Medium"
		case semiBold = "Poppins-SemiBold"
		case bold = "Poppins-Bold"
	}
	
	private var attributes: [NSAttributedString.Key: Any] = [:]
	
	init(font: Style, color: UIColor, size: CGFloat, alignment: NSTextAlignment? = nil) {
		guard let fontValue = UIFont(name: font.rawValue, size: size) else {
			return
		}
		
		var attributes = [
			NSAttributedString.Key.font: fontValue,
			NSAttributedString.Key.foregroundColor: color
		]
		
		if let alignment {
			let paragraphStyle = NSMutableParagraphStyle()
			paragraphStyle.alignment = alignment
			attributes.updateValue(paragraphStyle, forKey: NSAttributedString.Key.paragraphStyle)
		}
		
		self.attributes = attributes
	}
	
	func getAttributedString(from string: String) -> NSAttributedString {
		return NSAttributedString(string: string, attributes: self.attributes)
	}
	
	func setAttributes(on string: NSMutableAttributedString?, range: NSRange?) {
		guard let range else {
			return
		}
		
		string?.setAttributes(self.attributes, range: range)
	}
}
