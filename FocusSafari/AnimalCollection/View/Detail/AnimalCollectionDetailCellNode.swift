//
//  AnimalCollectionDetailCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 12/01/24.
//

import Foundation
import AsyncDisplayKit

final class AnimalCollectionDetailCellNode: ASCellNode {
	
	private let titleNode = ASTextNode()
	private let descriptionNode = ASTextNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var cellInset: CGFloat {
		return isSmallDevice ? 16 : 20
	}
	
	private let viewModel: AnimalCollectionDetailCellViewModel
	
	init(viewModel: AnimalCollectionDetailCellViewModel) {
		self.viewModel = viewModel
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let textStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 12,
			justifyContent: .start,
			alignItems: .start,
			children: [
				titleNode,
				viewModel.desc != nil ? descriptionNode : getMultiDescriptionNode()
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0), child: textStack)
	}
	
	private func configureNodes() {
		configureTitleNode()
		
		if viewModel.desc != nil {
			configureSingleDescriptionNode()
		}
	}
	
	private func configureTitleNode() {
		let titleStyle = FontStyle(font: .medium, color: .darkYellow, size: 18, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: viewModel.title)
	}
	
	private func configureSingleDescriptionNode() {
		let descriptionStyle = FontStyle(font: .regular, color: .white, size: 12, alignment: .left)
		descriptionNode.attributedText = descriptionStyle.getAttributedString(from: viewModel.desc ?? "")
		descriptionNode.style.minSize = CGSize(width: ScreenInfo.width - 2*cellInset, height: 0)
	}
	
	private func getMultiDescriptionNode() -> ASLayoutSpec {
		let descriptionStyle = FontStyle(font: .regular, color: .white, size: 12, alignment: .left)
		descriptionNode.attributedText = descriptionStyle.getAttributedString(from: viewModel.desc ?? "")
		descriptionNode.style.minSize = CGSize(width: ScreenInfo.width - 2*cellInset, height: 0)
		
		var children: [ASLayoutElement] = []
		
		viewModel.multiDesc?.forEach({ key, value in
			
			let keyTextNode = ASTextNode()
			keyTextNode.attributedText = descriptionStyle.getAttributedString(from: key)
			keyTextNode.style.preferredSize = CGSize(width: 100, height: 18)
			
			let divider = ASTextNode()
			divider.attributedText = descriptionStyle.getAttributedString(from: ":")
			divider.style.preferredSize = CGSize(width: 10, height: 18)
			
			let valueTextNode = ASTextNode()
			valueTextNode.attributedText = descriptionStyle.getAttributedString(from: value)
			valueTextNode.style.minSize = CGSize(width: ScreenInfo.width - 2*cellInset - 100 - 10, height: 0)
			valueTextNode.style.maxSize = CGSize(width: ScreenInfo.width - 2*cellInset - 100 - 10, height: 200)
			
			let stack = ASStackLayoutSpec(
				direction: .horizontal,
				spacing: 0,
				justifyContent: .start,
				alignItems: .start,
				children: [
					keyTextNode,
					divider,
					valueTextNode
				])
			
			children.append(ASInsetLayoutSpec(insets: .zero, child: stack))
		})
		
		return ASStackLayoutSpec(
			direction: .vertical,
			spacing: 4,
			justifyContent: .start,
			alignItems: .start,
			children: children)
	}
}
