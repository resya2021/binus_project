//
//  UIView.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 13/01/24.
//

import UIKit

extension UIView {
	func gradient(from color1: UIColor, to color2: UIColor, cornerRadius: CGFloat = 10) {
		DispatchQueue.main.async {
			
			let size = self.bounds.size
			let gradient: CAGradientLayer = CAGradientLayer()
			gradient.colors = [color1.cgColor, color2.cgColor]
			gradient.locations = [0.0 , 1.0]
			gradient.startPoint = CGPoint(x: 0.5, y: 0)
			gradient.endPoint = CGPoint(x: 0.5, y: 1)
			gradient.cornerRadius = cornerRadius
			gradient.frame = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
			if let sublayers = self.layer.sublayers, !sublayers.isEmpty {
				self.layer.insertSublayer(gradient, below: sublayers.first)
			} else {
				self.layer.insertSublayer(gradient, at: 0)
			}
		}
	}
}
