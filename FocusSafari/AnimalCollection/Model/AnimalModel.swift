//
//  AnimalModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation

struct AnimalModel {
	
	var name: String
	var id: String
	var rarity: EggRarity
	var extendedInfo: [String: Any]?
	
	var assetName: String {
		return "animal_\(self.name.lowercased().replacingOccurrences(of: " ", with: ""))"
	}
	
	init(name: String, id: String, rarity: EggRarity, extendedInfo: [String: Any]? = nil) {
		self.name = name
		self.id = id
		self.rarity = rarity
		self.extendedInfo = extendedInfo
	}
	
	init(dict: [String: Any]) {
		self.name = ((dict["name"] as? String) ?? "")
		self.id = ((dict["id"] as? String) ?? "")
		self.rarity = EggRarity(rawValue: ((dict["rarity"] as? Int) ?? 0)) ?? .common
		self.extendedInfo = (dict["extendedInfo"] as? [String: Any])
	}
}

