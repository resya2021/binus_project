//
//  HomeNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import Foundation
import AsyncDisplayKit
import ActivityKit
import SwiftUI
import RxSwift

final class HomeNode: ASDisplayNode {
	
	var onShowSoundButton: ((Bool) -> Void)?
	var onChangeDuration: (() -> Void)?
	var onGetReward: ((Int, Bool) -> Void)?
	var onShowWelcome: (() -> Void)?
	
	var timerRemaining: Int {
		didSet {
			updateTimerDuration()
		}
	}
	
	// UI Components
	private let titleNode: ASTextNode = ASTextNode()
	private let subtitleNode: ASTextNode = ASTextNode()
	private let timerWidget: TimerWidgetNode = TimerWidgetNode()
	private let timerTitleNode: ASTextNode = ASTextNode()
	private let timerDurationNode: ASTextNode = ASTextNode()
	private let startButton: ASButtonNode = ASButtonNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	// Live Activity
	private var _activity: Any? = nil
	
	@available(iOS 16.2, *)
	fileprivate var activity: Activity<FocusSafariAttributes>? {
		return _activity as? Activity<FocusSafariAttributes>
	}
	
	
	// Data Components
	private var endTime: Date?
	
	private var isTimerInterrupted = false
	private var lastUsedTimer: Int = 60
	
	private var elapsedTime: Int {
		return lastUsedTimer - timerRemaining
	}
	
	private var timer: Timer?
	private let timerFontStyle = FontStyle(font: .medium, color: .white, size: ScreenInfo.size == .small ? 64 : 80, alignment: .center)
	
	private let viewModel: HomeViewModel
	
	private let disposeBag: DisposeBag = DisposeBag()
	
	init(viewModel: HomeViewModel) {
		self.viewModel = viewModel
		self.timerRemaining = lastUsedTimer
		super.init()
		automaticallyManagesSubnodes = true
		gradient(from: .lightBlue, to: .darkBlue)
		configureNodes()
	}
	
	override func didLoad() {
		super.didLoad()
		
		presentWelcome()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		let inset: CGFloat = isSmallDevice ?  12 : 16
		let timerWidgetInset = ASInsetLayoutSpec(insets: UIEdgeInsets(top: inset, left: .infinity, bottom: inset, right: .infinity), child: timerWidget)
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: isSmallDevice ?  6 : 8,
			justifyContent: .center,
			alignItems: .center,
			children: [
				titleNode,
				subtitleNode,
				timerWidgetInset,
				timerTitleNode,
				timerDurationNode,
				startButton
			])
		
		return ASInsetLayoutSpec(insets: ScreenInfo.safeAreaInsets, child: stack)
	}
	
	private func presentWelcome() {
		
//		guard viewModel.isFirstTime else {
//			return
//		}
		
		onShowWelcome?()
		UserDefaults.standard.set(false, forKey: UserDefaultsKey.isFirstOpen.rawValue)
	}
	
	private func startTimer() {
		guard viewModel.screenTimeAuthorizationStatus.value == .approved else {
			viewModel.requestScreenTimePermission()
			observeAuthorizationStatus()
			return
		}
		
		setCancelButton()
		lastUsedTimer = timerRemaining
		timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
		
		endTime = Date(timeIntervalSinceNow: TimeInterval(timerRemaining))
		timerWidget.startAnimate(duration: timerRemaining, until: endTime)
		addObserver()
		onShowSoundButton?(true)
		showTextNodes(false)
		
		if #available(iOS 16.2, *) {
			startLiveActivity()
		}
	}
	
	@objc
	private func resumeTimer() {
		guard let endTime, .now < endTime else {
			stopTimer()
			return
		}
		
		timerRemaining = Int(endTime.timeIntervalSinceNow)
		timer?.invalidate()
		timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
	}
	
	private func stopTimer() {
		guard let timer = self.timer else {
			return
		}
		
		if #available(iOS 16.2, *) {
			Task {
				await endLiveActivity()
			}
		}
		
		timer.invalidate()
		self.timer = nil
		
		setDefaultStart()
		timerWidget.stopAnimate()
		removeObserver()
		onShowSoundButton?(false)
		showTextNodes(true)
		onGetReward?(elapsedTime/60, isTimerInterrupted)
		self.timerRemaining = lastUsedTimer
	}
	
	@objc
	private func updateTimer() {
		if timerRemaining > 0 {
			timerRemaining -= 1  // decrease counter timer
		} else {
			stopTimer()
		}
		
		let timerText = getTimeFormatted(from: timerRemaining)
		self.timerDurationNode.attributedText = timerFontStyle.getAttributedString(from: timerText)
	}
	
	private func getTimeFormatted(from totalSeconds: Int) -> String {
		let seconds: Int = totalSeconds % 60
		let minutes: Int = (totalSeconds / 60) % 60
		return String(format: "%02d:%02d", minutes, seconds)
	}
	
	private func configureNodes() {
		configureTextNodes()
		configureWidgetNode()
		configureButtonNode()
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  20 : 24, alignment: .center)
		titleNode.attributedText = titleStyle.getAttributedString(from: "Start Focus Today!")
		
		let subtitleStyle = FontStyle(font: .regularItalic, color: .white, size: isSmallDevice ?  14 : 16, alignment: .center)
		subtitleNode.attributedText = subtitleStyle.getAttributedString(from: "Get The Most Out Of Your Time!")
		
		let timerTitleStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  14 : 16, alignment: .center)
		timerTitleNode.attributedText = timerTitleStyle.getAttributedString(from: "Duration")
		
		timerDurationNode.isUserInteractionEnabled = true
		timerDurationNode.addTarget(self, action: #selector(changeDuration), forControlEvents: .touchUpInside)
		updateTimerDuration()
	}
	
	private func showTextNodes(_ shouldShow: Bool) {
		titleNode.isHidden = !shouldShow
		subtitleNode.isHidden = !shouldShow
	}
	
	private func updateTimerDuration() {
		timerDurationNode.attributedText = timerFontStyle.getAttributedString(from: getTimeFormatted(from: timerRemaining))
	}
	
	private func configureWidgetNode() {
		let size = isSmallDevice ?  240 : 300
		timerWidget.style.preferredSize = CGSize(width: size, height: size)
	}
	
	private func configureButtonNode() {
		startButton.style.preferredSize = CGSize(width: isSmallDevice ?  200 : 250, height: isSmallDevice ?  48 : 60)
		startButton.cornerRadius = 10
		startButton.addTarget(self, action: #selector(buttonPressed), forControlEvents: .touchUpInside)
		startButton.gradient(from: .lightYellow, to: .darkYellow)
		
		setDefaultStart()
	}
	
	private func setDefaultStart() {
		let buttonStyle = FontStyle(font: .semiBold, color: .deepTeal, size: isSmallDevice ?  16 : 20, alignment: .center)
		startButton.setAttributedTitle(buttonStyle.getAttributedString(from: "START!"),
									   for: .normal)
		startButton.replaceGradient(from: .lightYellow, to: .darkYellow)
	}
	
	private func setCancelButton() {
		let buttonStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  16 : 20, alignment: .center)
		startButton.setAttributedTitle(buttonStyle.getAttributedString(from: "CANCEL"),
									   for: .normal)
		startButton.replaceGradient(from: .lightCyan, to: .darkCyan)
	}
	
	@objc
	private func changeDuration() {
		onChangeDuration?()
	}
	
	@objc
	private func buttonPressed() {
		if let _ = self.timer {
			isTimerInterrupted = true
			stopTimer()
		} else {
			startTimer()
		}
	}
	
	private func addObserver() {
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(self.resumeTimer),
			name: UIApplication.willEnterForegroundNotification,
			object: nil
		)
	}
	
	private func removeObserver() {
		NotificationCenter.default.removeObserver(
			self,
			name: UIApplication.willEnterForegroundNotification,
			object: nil
		)
	}
	
	private func observeAuthorizationStatus() {
		viewModel.screenTimeAuthorizationStatus.asObservable()
			.take(until: { $0 == .approved}, behavior: .inclusive)
			.filter({$0 == .approved})
			.subscribe(onNext: { [weak self] _ in
				self?.startTimer()
			}).disposed(by: disposeBag)
	}
	
	@available(iOS 16.2, *)
	private func startLiveActivity() {
		guard let time = self.endTime else {
			return
		}
		
		let attributes = FocusSafariAttributes(iconName: "logo")
		let contentState = FocusSafariAttributes.ContentState(endTime: time)
		let activityContent = ActivityContent(state: contentState, staleDate: endTime)

		do {
			let activity = try Activity<FocusSafariAttributes>.request(attributes: attributes, content: activityContent)
			self._activity = activity
		} catch {
			print("Error starting live activity: \(error)")
		}
	}
	
	@available(iOS 16.2, *)
	private func endLiveActivity() async {
		guard let liveActivity = self.activity else {
			return
		}
		
		let contentState = FocusSafariAttributes.ContentState(message: "Success!")
		await liveActivity.end(using: contentState, dismissalPolicy: .immediate)
	}
}
