//
//  FeatureCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 11/01/24.
//

import Foundation
import AsyncDisplayKit

final class FeatureCellNode: ASDisplayNode {
	
	private let iconNode = ASImageNode()
	private let textNode = ASTextNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var cellInset: CGFloat {
		return isSmallDevice ? 20 : 30
	}
	
	private let title: String
	private let desc: String
	
	init(title: String, desc: String) {
		self.title = title
		self.desc = desc
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let textStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 12,
			justifyContent: .start,
			alignItems: .start,
			children: [
				iconNode,
				textNode
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets.zero, child: textStack)
	}
	
	private func configureNodes() {
		configureImageNode()
		configureTextNode()
	}
	
	private func configureImageNode() {
		iconNode.image = UIImage(named: "icon_round_arrow")
		iconNode.style.preferredSize = CGSize(width: isSmallDevice ?  12 : 14, height: isSmallDevice ?  12 : 14)
		iconNode.contentMode = .scaleAspectFit
	}
	
	private func configureTextNode() {
		textNode.style.minSize = CGSize(width: ScreenInfo.width - 16 - 12 - 2*cellInset, height: 0)
		textNode.style.maxSize = CGSize(width: ScreenInfo.width - 16 - 12 - 2*cellInset, height: 100)
		
		let captionStyle = FontStyle(font: .regular, color: .white, size: isSmallDevice ?  12 : 14, alignment: .left)
		textNode.attributedText = captionStyle.getAttributedString(from: title + desc)
		
		let titleStyle = FontStyle(font: .bold, color: .white, size: isSmallDevice ?  12 : 14, alignment: .left)
		let titleRange = (textNode.attributedText?.string as? NSString)?.range(of: title)
		
		titleStyle.setAttributes(on: textNode.attributedText as? NSMutableAttributedString, range: titleRange)
		
	}
}
