//
//  ShopViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 04/01/24.
//

import Foundation
import AsyncDisplayKit

final class ShopViewController: ASDKViewController<ASDisplayNode> {
	
	private var mainNode: ShopNode?
	private var viewModel: ShopViewModel?
	private var isFirstDidLayoutSubviews: Bool = true
	
	init(viewModel: ShopViewModel) {
		let mainNode = ShopNode(viewModel: viewModel)
		self.mainNode = mainNode
		self.viewModel = viewModel
		
		super.init(node: mainNode)
		
		configureMainNode()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureNavigationBar()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		if isFirstDidLayoutSubviews {
			mainNode?.gradient(from: .lightBlue, to: .darkBlue)
			navigationItem.rightBarButtonItem?.customView?.gradient(from: .white, to: .gradientGray, cornerRadius: 5)
			isFirstDidLayoutSubviews = false
		}
	}
	
	private func configureNavigationBar() {
		
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationController?.navigationBar.shadowImage = UIImage()
		
		let titleLabel = UILabel()
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: 20)
		titleLabel.attributedText = titleStyle.getAttributedString(from: "Shop")
		
		navigationItem.titleView = titleLabel
		
		let backButton = UIButton(type: .custom)
		backButton.setImage(UIImage(named: "icon_back_white"), for: .normal)
		backButton.addTarget(self, action: #selector(popViewController), for: .touchUpInside)
		navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
		
		let gemInfoNode = GemInfoNode()
		gemInfoNode.cornerRadius = 5
		gemInfoNode.frame = CGRect(x: 0, y: 0, width: 130, height: 40)
		gemInfoNode.layoutIfNeeded()
		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: gemInfoNode.view)
	}
	
	private func configureMainNode() {
		mainNode?.onHatch = { [weak self] rarity in
			
			self?.getRandomAnimal(with: rarity)
			
			let alertController = UIAlertController(title: "Hatched!", message: "You get an animal!", preferredStyle: UIAlertController.Style.alert)
			let confirmAction = UIAlertAction(title: "Go To Collection", style: UIAlertAction.Style.default) { _ in
				let collectionVC = AnimalCollectionViewController(viewModel: AnimalCollectionViewModel())
				
				collectionVC.onPush = { [weak self] vc in
					self?.navigationController?.pushViewController(vc, animated: true)
				}
				
				self?.navigationController?.pushViewController(collectionVC, animated: true)
			}
			
			alertController.addAction(confirmAction)
			self?.present(alertController, animated: true, completion:{})
		}
		
		mainNode?.onGemNotEnough = { [weak self] in
			
			let alertController = UIAlertController(title: "Sorry!", message: "You don't have enough gem.", preferredStyle: UIAlertController.Style.alert)
			let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
				//
			}
			
			alertController.addAction(confirmAction)
			self?.present(alertController, animated: true, completion:{})
		}
	}
	
	private func getRandomAnimal(with rarity: EggRarity) {
		let animalId = generateRandomAnimalId(with: rarity)
		addNewAnimalToCollection(animalId: animalId)
	}
	
	private func generateRandomAnimalId(with rarity: EggRarity) -> String {
		guard let ownedAnimals = UserService.shared.userInfo.value?.ownedAnimals else {
			return ""
		}
		
		var range = 1...25
		
		switch rarity {
		case .mythical:
			range = 1...3
		case .rare:
			range = 4...12
		case .common:
			range = 13...25
		}
		
		let randomInt = Int.random(in: range)
		let animalId = "a0\(String(format: "%02d", randomInt))"
		
		if ownedAnimals.count > 24 {
			return ""
		} else if ownedAnimals.contains(animalId) {
			return generateRandomAnimalId(with: rarity)
		}
		
		return animalId
	}
	
	private func addNewAnimalToCollection(animalId: String) {
		guard var ownedAnimals = UserService.shared.userInfo.value?.ownedAnimals else {
			return
		}
		
		ownedAnimals.append(animalId)
		UserService.shared.updateUserInfo(with: ownedAnimals, for: .ownedAnimalsIds)
	}
	
	@objc
	private func popViewController() {
		navigationController?.popViewController(animated: true)
	}
}
