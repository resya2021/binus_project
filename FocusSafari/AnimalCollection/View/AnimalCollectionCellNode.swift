//
//  AnimalCollectionCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation
import AsyncDisplayKit

final class AnimalCollectionCellNode: ASCellNode {
	
	var onOpenDetail: ((AnimalModel) -> Void)?
	
	private let imageNode = ASImageNode()
	private let titleNode = ASTextNode()
	private let bgNode = ASDisplayNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private let animal: AnimalModel
	
	init(animal: AnimalModel) {
		self.animal = animal
		super.init()
		backgroundColor = .clear
		automaticallyManagesSubnodes = true
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let imageInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4),
											   child: imageNode)
		
		bgNode.cornerRadius = 5

		let bgSpec = ASBackgroundLayoutSpec(child: imageInsetSpec, background: bgNode)
		
		let mainStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 6,
			justifyContent: .center,
			alignItems: .center,
			children: [
				bgSpec,
				titleNode
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets.zero,
								 child: mainStack)
	}
	
	func setGradient() {
		bgNode.gradient(from: .white, to: .gradientGray)
	}
	
	private func configureNodes() {
		configureImageNodes()
		configureTextNodes()
	}
	
	private func configureImageNodes() {
		imageNode.image = UIImage(named: animal.assetName)
		imageNode.style.preferredSize = CGSize(width: 96, height: 96)
		imageNode.contentMode = .scaleAspectFit
		imageNode.isUserInteractionEnabled = true
		imageNode.addTarget(self, action: #selector(openDetail), forControlEvents: .touchUpInside)
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  10 : 12, alignment: .center)
		titleNode.attributedText = titleStyle.getAttributedString(from: animal.name)
	}
	
	@objc
	private func openDetail() {
		onOpenDetail?(self.animal)
	}
}

