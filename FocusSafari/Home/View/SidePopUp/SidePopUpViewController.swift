//
//  SidePopUpViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 25/12/23.
//

import Foundation
import AsyncDisplayKit

final class SidePopUpViewController: ASDKViewController<ASDisplayNode> {
	
	var onNavigate: ((NavigationEvent) -> Void)?
	
	private let mainNode: SidePopUpNode = SidePopUpNode()
	
	override init() {
		super.init(node: mainNode)
		
		configureMainNode()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func configureMainNode() {
		mainNode.onNavigate = { [weak self] event in
			self?.dismiss(animated: true)
			self?.onNavigate?(event)
		}
	}
}
