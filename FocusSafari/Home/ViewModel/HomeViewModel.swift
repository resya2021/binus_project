//
//  HomeViewModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import Foundation
import RxRelay
import FamilyControls

final class HomeViewModel {
	
	var isFirstTime: Bool {
		return UserService.shared.isFirstTime
	}
	
	let screenTimeAuthorizationStatus: BehaviorRelay<AuthorizationStatus> = BehaviorRelay<AuthorizationStatus>(value: AuthorizationCenter.shared.authorizationStatus)
	
	init() {
		
	}
	
	func requestScreenTimePermission() {
		guard screenTimeAuthorizationStatus.value != .approved else {
			return
		}
		
//		//MOCK for simulator
		screenTimeAuthorizationStatus.accept(.approved)
		
		if #available(iOS 16.0, *) {
			Task {
				await requestAuthorizationAsync()
			}
		} else {
			AuthorizationCenter.shared.requestAuthorization { result in
				//
			}
		}
	}
	
	@available(iOS 16.0, *)
	private func requestAuthorizationAsync() async {
		do {
			try await AuthorizationCenter.shared.requestAuthorization(for: .individual)
		} catch {
			print("Failed to enroll with error: \(error)")
		}
	}
}
