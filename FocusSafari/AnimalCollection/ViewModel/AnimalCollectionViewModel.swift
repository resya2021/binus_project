//
//  AnimalCollectionViewModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 04/01/24.
//

import Foundation

final class AnimalCollectionViewModel {
	
	var ownedAnimals: [AnimalModel] = []
	
	private var allAnimalsData: [String: [[String: Any]]]?
	
	init() {
		getDataFromJSON { [weak self] in
			self?.getOwnedAnimals()
		}
	}
	
	private func getOwnedAnimals() {
		
		guard let animalsData = self.allAnimalsData?["data"],
			  let ownedAnimalsId = UserService.shared.userInfo.value?.ownedAnimals else {
			return
		}
		
		
		let ownedAnimalsData = animalsData.filter { animal in
			guard let animalId = animal["id"] as? String,
				  ownedAnimalsId.contains(animalId) else {
				return false
			}
			
			return true
		}
		
		ownedAnimals = ownedAnimalsData.compactMap { animal -> AnimalModel? in
			guard let name = animal["name"] as? String,
				  let id = animal["id"] as? String,
				  let rarity = animal["rarity"] as? Int,
				  let animalRarity = EggRarity(rawValue: rarity) else {
				return nil
			}
			
			let info = animal["extendedInfo"] as? [String: Any]
			return AnimalModel(name: name, id: id, rarity: animalRarity, extendedInfo: info)
		}
	}

	private func getDataFromJSON(completion: (() -> Void)?) {
		guard let jsonFilePath = Bundle.main.path(forResource: "Animals", ofType: "json") else {
			completion?()
			return
		}
		
		do {
			let data = try Data(contentsOf: URL(fileURLWithPath: jsonFilePath))
			let animals = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: [[String: Any]]]
			self.allAnimalsData = animals
			completion?()
		} catch let error {
			print("Failed reading Animals.json \(error)")
			completion?()
		}
	}

}
