//
//  AppDelegate.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		self.window = UIWindow(frame: UIScreen.main.bounds)
		let homeViewController = HomeViewController(viewModel: HomeViewModel())
		let navigationController = UINavigationController(rootViewController: homeViewController)
		self.window?.rootViewController = navigationController
		self.window?.makeKeyAndVisible()
		
		return true
	}
	
}

