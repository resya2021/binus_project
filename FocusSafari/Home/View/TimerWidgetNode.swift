//
//  TimerWidgetNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import Foundation
import AsyncDisplayKit
import Lottie

final class TimerWidgetNode: ASDisplayNode {
	
	private let image: ASImageNode = ASImageNode()
	private let timerIndicator = LottieAnimationView(name: "TimerIndicator")
	private var duration: Int = 0
	private var endDate: Date?
	
	private lazy var lottieTimerNode: ASDisplayNode = {
		let node = ASDisplayNode()
		node.setViewBlock({ [weak self] () -> UIView in
			return self?.timerIndicator ?? UIView()
		})
		
		return node
	}()
	
	override init() {
		super.init()
		automaticallyManagesSubnodes = true
		
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		let imageInset = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16), child: image)
		
		let imageSpec = ASCenterLayoutSpec(horizontalPosition: .center, verticalPosition: .center, sizingOption: [], child: imageInset)
		
		let indicatorSpec = ASCenterLayoutSpec(horizontalPosition: .center, verticalPosition: .center, sizingOption: [], child: lottieTimerNode)
		
		let absoluteSpec = ASAbsoluteLayoutSpec(children: [imageSpec, indicatorSpec])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets.zero, child: absoluteSpec)
	}
	
	func startAnimate(duration: Int, until endDate: Date?) {
		self.duration = duration
		self.endDate = endDate
		
		timerIndicator.animationSpeed = 4/CGFloat(integerLiteral: duration)
		timerIndicator.play(toFrame: 240) { [weak self] _ in
			self?.timerIndicator.animationSpeed = 1
			self?.timerIndicator.play(fromFrame: 241, toFrame: 300) { [weak self] _ in
				self?.timerIndicator.stop()
			}
		}
		
		addObserver()
	}
	
	func pauseAnimate() {
		timerIndicator.pause()
	}
	
	func stopAnimate() {
		timerIndicator.stop()
		removeObserver()
	}
	
	private func configureNodes() {
		image.image = UIImage(named: "timer")
		image.style.preferredSize = CGSize(width: 290, height: 290)
		
		timerIndicator.frame = CGRect(x: self.frame.midX, y: self.frame.midY, width: 320, height: 320)
		timerIndicator.contentMode = .scaleAspectFit
	}
	
	private func addObserver() {
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(self.syncTimer),
			name: UIApplication.willEnterForegroundNotification,
			object: nil
		)
	}
	
	private func removeObserver() {
		NotificationCenter.default.removeObserver(
			self,
			name: UIApplication.willEnterForegroundNotification,
			object: nil
		)
	}
	
	@objc
	private func syncTimer() {
		guard let remainingTime = endDate?.timeIntervalSinceNow else {
			return
		}
		
		let elapsedDuration = CGFloat(integerLiteral: duration) - CGFloat(remainingTime)
		let timerProgress = elapsedDuration / CGFloat(integerLiteral: duration)
		let continueFrame = timerProgress * 240
		
		guard timerProgress < 1.0 else {
			return
		}
		
		timerIndicator.stop()
		timerIndicator.animationSpeed = 4/CGFloat(integerLiteral: duration)
		timerIndicator.play(fromFrame: continueFrame, toFrame: 240) { [weak self] _ in
			self?.timerIndicator.animationSpeed = 1
			self?.timerIndicator.play(fromFrame: 241, toFrame: 300) { [weak self] _ in
				self?.timerIndicator.stop()
			}
		}
	}
	
}
