//
//  SidePopUpNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 25/12/23.
//

import Foundation
import AsyncDisplayKit

enum NavigationEvent {
	case home
	case collection
	case shop
	case account
	case about
}

final class SidePopUpNode: ASDisplayNode {
	
	var onNavigate: ((NavigationEvent) -> Void)?
	
	private lazy var collectionNode: ASCollectionNode = {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .vertical
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		
		let collectionNode = ASCollectionNode(collectionViewLayout: layout)
		
		return collectionNode
	}()
	
	override init() {
		super.init()
		automaticallyManagesSubnodes = true
		
		configureCollectionNode()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: .zero, left: .infinity, bottom: .zero, right: .zero),
								 child: collectionNode)
	}
	
	private func configureCollectionNode() {
		collectionNode.dataSource = self
		collectionNode.delegate = self
		collectionNode.backgroundColor = .white
		collectionNode.view.contentInsetAdjustmentBehavior = .never
	}
}
	
extension SidePopUpNode: ASCollectionDataSource, ASCollectionDelegate {
	func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
		return 1
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
		return 5
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
		return {
			
			guard indexPath.row < 5 else {
				return ASCellNode()
			}
			
			var buttonImageName: String = ""
			var buttonTitle: String = ""
			var buttonAction: NavigationEvent = .home
			
			switch indexPath.row {
			case 0:
				buttonImageName = "home"
				buttonTitle = "Home"
				buttonAction = .home
			case 1:
				buttonImageName = "paw"
				buttonTitle = "Animal Collection"
				buttonAction = .collection
			case 2:
				buttonImageName = "shop"
				buttonTitle = "Shop"
				buttonAction = .shop
			case 3:
				buttonImageName = "account"
				buttonTitle = "Account"
				buttonAction = .account
			case 4:
				buttonImageName = "about"
				buttonTitle = "About Focus Safari"
				buttonAction = .about
			default:
				break
			}
			
			let cellNode = SidePopUpCellNode(buttonIcon: "icon_\(buttonImageName)",
											 buttonTitle: buttonTitle,
											 buttonAction: buttonAction)
			
			cellNode.style.preferredSize = CGSize(width: 226, height: 50)
			
			return cellNode
		}
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
		guard let cell = collectionNode.nodeForItem(at: indexPath) as? SidePopUpCellNode else {
			return
		}
		
		onNavigate?(cell.buttonAction)
	}
}
