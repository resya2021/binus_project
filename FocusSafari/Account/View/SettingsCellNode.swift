//
//  SettingsCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 10/01/24.
//

import Foundation
import AsyncDisplayKit

final class SettingsCellNode: ASCellNode {
	
	var buttonAction: SettingsNavigation
	
	private let separatorNode = ASDisplayNode()
	
	private let imageNode = ASImageNode()
	private let titleNode = ASTextNode()
	private let arrowNode = ASImageNode()
	
	private let buttonIcon: String?
	private let buttonTitle: String
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var cellInset: CGFloat {
		return isSmallDevice ?  16 : 20
	}
	
	init(buttonIcon: String?, buttonTitle: String, buttonAction: SettingsNavigation) {
		self.buttonIcon = buttonIcon
		self.buttonTitle = buttonTitle
		self.buttonAction = buttonAction
		
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let contentStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 10,
			justifyContent: .spaceBetween,
			alignItems: .center,
			children: [
				imageNode,
				titleNode,
				arrowNode
			])
		
		contentStack.style.minSize = CGSize(width: ScreenInfo.width - 2*cellInset - 2*8, height: 0)
		
		let insetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 8, bottom: 0, right: 8), child: contentStack)
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 0,
			justifyContent: .start,
			alignItems: .center,
			children: [
				separatorNode,
				insetSpec
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0), child: stack)
	}
	
	private func configureNodes() {
		configureImageNodes()
		configureTextNodes()
		configureSeparatorNode()
	}
	
	private func configureImageNodes() {
		guard let buttonIcon else {
			return
		}
		
		imageNode.image = UIImage(named: buttonIcon)
		imageNode.style.preferredSize = CGSize(width: 25, height: 25)
		imageNode.contentMode = .scaleAspectFit
		
		arrowNode.image = UIImage(named: "icon_next_white")
		arrowNode.style.preferredSize = CGSize(width: 20, height: 20)
		arrowNode.contentMode = .scaleAspectFit
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .medium, color: .white, size: 14, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: buttonTitle)
		titleNode.style.flexGrow = 1
	}
	
	private func configureSeparatorNode() {
		separatorNode.backgroundColor = .white
		separatorNode.style.minSize = CGSize(width: 0, height: 1)
		separatorNode.style.maxSize = CGSize(width: ScreenInfo.width, height: 1)
	}
}

