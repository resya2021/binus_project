//
//  ShopNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 04/01/24.
//

import Foundation
import AsyncDisplayKit

final class ShopNode: ASDisplayNode {
	
	var onHatch: ((EggRarity) -> Void)?
	var onGemNotEnough: (() -> Void)?
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private let viewModel: ShopViewModel
	
	private lazy var collectionNode: ASCollectionNode = {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .vertical
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		
		let collectionNode = ASCollectionNode(collectionViewLayout: layout)
		
		return collectionNode
	}()
	
	init(viewModel: ShopViewModel) {
		self.viewModel = viewModel
		super.init()
		automaticallyManagesSubnodes = true
		
		configureCollectionNode()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: ScreenInfo.safeAreaInsets.top + 30 + 24,
													  left: 0,
													  bottom: ScreenInfo.safeAreaInsets.bottom,
													  right: 0),
								 child: collectionNode)
	}
	
	private func configureCollectionNode() {
		collectionNode.dataSource = self
		collectionNode.delegate = self
		collectionNode.backgroundColor = .clear
		collectionNode.view.contentInsetAdjustmentBehavior = .never
	}
}

extension ShopNode: ASCollectionDataSource, ASCollectionDelegate {
	func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
		return 1
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
		return 3
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
		return { [weak self] in
			guard let eggRarity = EggRarity(rawValue: indexPath.row) else {
				return ASCellNode()
			}
			
			let itemModel = ShopItemModel(rarity: eggRarity)
			let cell = ShopCellNode(item: itemModel)
			
			cell.onHatch = { [weak self] eggRarity in
				if let isGemEnough = self?.viewModel.isGemEnough(for: itemModel.price), isGemEnough {
					self?.viewModel.deductGem(for: itemModel.price)
					self?.onHatch?(eggRarity)
				} else {
					self?.onGemNotEnough?()
				}
			}
			
			return cell
		}
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, willDisplayItemWith node: ASCellNode) {
		guard let cellNode = node as? ShopCellNode else {
			return
		}
		
		cellNode.setGradient()
	}
}
