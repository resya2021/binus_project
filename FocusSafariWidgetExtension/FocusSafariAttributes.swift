//
//  FocusSafariAttributes.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 02/01/24.
//

import ActivityKit
import SwiftUI
import WidgetKit

@available(iOS 16.2, *)
struct FocusSafariAttributes: ActivityAttributes {
	public struct ContentState: Codable, Hashable {
		var endTime: Date?
		var message: String?
	}

	var iconName: String
}

