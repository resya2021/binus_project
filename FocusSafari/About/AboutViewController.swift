//
//  AboutViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 11/01/24.
//

import Foundation
import AsyncDisplayKit

final class AboutViewController: ASDKViewController<ASDisplayNode> {
	
	private var mainNode: AboutNode
	
	override init() {
		self.mainNode = AboutNode()
		super.init(node: mainNode)
		
		configureMainNode()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureNavigationBar()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		mainNode.gradient(from: .lightBlue, to: .darkBlue)
	}
	
	private func configureNavigationBar() {
		
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationController?.navigationBar.shadowImage = UIImage()
		
		let titleLabel = UILabel()
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: 20)
		titleLabel.attributedText = titleStyle.getAttributedString(from: "About Focus Safari")
		
		navigationItem.titleView = titleLabel
		
		let backButton = UIButton(type: .custom)
		backButton.setImage(UIImage(named: "icon_back_white"), for: .normal)
		backButton.addTarget(self, action: #selector(popViewController), for: .touchUpInside)
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
	}
	
	private func configureMainNode() {
		mainNode.onDone = { [weak self] in
			self?.dismiss(animated: true)
		}
	}
	
	@objc
	private func popViewController() {
		navigationController?.popViewController(animated: true)
	}
}

