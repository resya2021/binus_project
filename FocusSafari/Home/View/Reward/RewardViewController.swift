//
//  RewardViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 09/01/24.
//

import Foundation
import AsyncDisplayKit

final class RewardViewController: ASDKViewController<ASDisplayNode> {
	
	private let mainNode: RewardNode
	
	init(viewModel: RewardViewModel) {
		self.mainNode = RewardNode(viewModel: viewModel)
		super.init(node: mainNode)
		
		configureMainNode()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func configureMainNode() {
		mainNode.onClose = { [weak self] in
			self?.dismiss(animated: true)
		}
	}
}

