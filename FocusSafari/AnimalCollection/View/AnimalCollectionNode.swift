//
//  AnimalCollectionNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 04/01/24.
//

import Foundation
import AsyncDisplayKit

final class AnimalCollectionNode: ASDisplayNode {
	
	var onOpenDetail: ((AnimalModel) -> Void)?
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private let viewModel: AnimalCollectionViewModel
	
	private lazy var collectionNode: ASCollectionNode = {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .vertical
		layout.minimumLineSpacing = isSmallDevice ?  8 : 10
		layout.minimumInteritemSpacing = isSmallDevice ?  8 : 10
		layout.headerReferenceSize = CGSize(width: 250, height: 30)
		
		let collectionNode = ASCollectionNode(collectionViewLayout: layout)
		
		return collectionNode
	}()
	
	init(viewModel: AnimalCollectionViewModel) {
		self.viewModel = viewModel
		super.init()
		automaticallyManagesSubnodes = true
		
		configureCollectionNode()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: ScreenInfo.safeAreaInsets.top + 30 + 24,
													  left: 10,
													  bottom: ScreenInfo.safeAreaInsets.bottom,
													  right: 10),
								 child: collectionNode)
	}
	
	private func configureCollectionNode() {
		collectionNode.dataSource = self
		collectionNode.delegate = self
		collectionNode.backgroundColor = .clear
		collectionNode.view.contentInsetAdjustmentBehavior = .never
	}
}

extension AnimalCollectionNode: ASCollectionDataSource, ASCollectionDelegate {
	func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
		return EggRarity.allCases.count
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
		guard let eggRarityWithReversedSorting = EggRarity(rawValue: (EggRarity.allCases.count - 1) - section) else {
			return 0
		}
		
		let ownedRarities = viewModel.ownedAnimals.filter({$0.rarity == eggRarityWithReversedSorting}).count
		
		return ownedRarities == 0 ? 1 : ownedRarities
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
		return { [weak self] in
			guard let eggRarityWithReversedSorting = EggRarity(rawValue: (EggRarity.allCases.count - 1) - indexPath.section),
				  let rarityFilteredAnimals = self?.viewModel.ownedAnimals.filter({$0.rarity == eggRarityWithReversedSorting}) else {
				return ASCellNode()
			}
			
			if rarityFilteredAnimals.isEmpty {
				return EmptySectionPlaceholder(rarity: eggRarityWithReversedSorting)
			}
			
			let cell = AnimalCollectionCellNode(animal: rarityFilteredAnimals[indexPath.row])
			
			cell.onOpenDetail = { [weak self] animal in
				self?.onOpenDetail?(animal)
			}
			
			return cell
		}
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, willDisplayItemWith node: ASCellNode) {
		guard let cellNode = node as? AnimalCollectionCellNode else {
			return
		}
		
		cellNode.setGradient()
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, supplementaryElementKindsInSection section: Int) -> [String] {
		return [UICollectionView.elementKindSectionHeader]
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> ASCellNodeBlock {
		return { [weak self] in
			guard let eggRarityWithReversedSorting = EggRarity(rawValue: (EggRarity.allCases.count - 1) - indexPath.section),
				  let rarityFilteredAnimalsCount = self?.viewModel.ownedAnimals.filter({$0.rarity == eggRarityWithReversedSorting}).count else {
				return ASCellNode()
			}
			
			let cellNode = ASCellNode()
			cellNode.automaticallyManagesSubnodes = true
			cellNode.layoutSpecBlock = { [weak self] _, _ in
				guard let self else {
					return ASLayoutSpec()
				}
				
				let textNode = ASTextNode()
				let textStyle = FontStyle(font: .semiBold, color: .white, size: 18, alignment: .left)
				textNode.attributedText = textStyle.getAttributedString(from: "\(eggRarityWithReversedSorting.string) Animals (\(rarityFilteredAnimalsCount))")
				
				
				return ASInsetLayoutSpec(insets: .zero, child: textNode)
			}
			
			return cellNode
		}
	}
}
