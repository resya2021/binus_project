//
//  BioCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 10/01/24.
//

import Foundation
import AsyncDisplayKit

final class BioCellNode: ASCellNode {
	
	var onEdit: (() -> Void)?
	
	private let titleNode = ASTextNode()
	private let descriptionNode = ASTextNode()
	
	private let containerNode = ASDisplayNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var cellInset: CGFloat {
		return isSmallDevice ?  16 : 20
	}
	
	private var stackSpacing: CGFloat {
		return isSmallDevice ?  10 : 12
	}
	
	private let viewModel: AccountViewModel
	
	init(viewModel: AccountViewModel) {
		self.viewModel = viewModel
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func didLoad() {
		super.didLoad()
		
		addTapGestureRecognizer()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let insetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: stackSpacing,
															   left: stackSpacing,
															   bottom: stackSpacing,
															   right: stackSpacing),
										  child: descriptionNode)
		
		let bgSpec = ASBackgroundLayoutSpec(child: insetSpec, background: containerNode)
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: stackSpacing,
			justifyContent: .start,
			alignItems: .start,
			children: [
				titleNode,
				bgSpec
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8,
													  left: 0,
													  bottom: 0,
													  right: 0),
								 child: stack)
	}
	
	private func configureNodes() {
		configureTextNodes()
		configureContainerNode()
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  14 : 16, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: "Ranger Bio")
		
		let descriptionStyle = FontStyle(font: .regularItalic, color: .white, size: isSmallDevice ?  12 : 14, alignment: .left)
		descriptionNode.attributedText = descriptionStyle.getAttributedString(from: viewModel.bio)
		descriptionNode.maximumNumberOfLines = 5
		descriptionNode.style.minSize = CGSize(width: ScreenInfo.width - 2*cellInset - 2*stackSpacing, height: 0)
	}
	
	private func configureContainerNode() {
		containerNode.cornerRadius = 10
		containerNode.borderColor = UIColor.white.cgColor
		containerNode.borderWidth = 2
	}
	
	private func addTapGestureRecognizer() {
		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(editBio))
		descriptionNode.view.addGestureRecognizer(tapGesture)
		descriptionNode.view.isUserInteractionEnabled = true
	}
	
	@objc
	private func editBio() {
		onEdit?()
	}
}
