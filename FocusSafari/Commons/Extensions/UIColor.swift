//
//  UIColor.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import UIKit

extension UIColor {
	static let lightBlue = UIColor(red: 8/255, green: 50/255, blue: 79/255, alpha: 1)
	static let darkBlue = UIColor(red: 2/255, green: 16/255, blue: 26/255, alpha: 1)
	static let lightYellow = UIColor(red: 1, green: 225/255, blue: 112/255, alpha: 1)
	static let darkYellow = UIColor(red: 245/255, green: 161/255, blue: 21/255, alpha: 1)
	static let teal = UIColor(red: 0, green: 80/255, blue: 136/255, alpha: 1)
	static let deepTeal = UIColor(red: 10/255, green: 48/255, blue: 74/255, alpha: 1)
	static let gray61 = UIColor(red: 61/255, green: 61/255, blue: 61/255, alpha: 1)
	static let lightCyan = UIColor(red: 105/255, green: 211/255, blue: 235/255, alpha: 1)
	static let darkCyan = UIColor(red: 17/255, green: 110/255, blue: 131/255, alpha: 1)
	static let darkNavy = UIColor(red: 2/255, green: 16/255, blue: 26/255, alpha: 0.7)
	static let gradientGray = UIColor(red: 143/255, green: 143/255, blue: 143/255, alpha: 1)
}
