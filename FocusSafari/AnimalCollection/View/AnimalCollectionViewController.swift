//
//  AnimalCollectionViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 04/01/24.
//

import Foundation
import AsyncDisplayKit

final class AnimalCollectionViewController: ASDKViewController<ASDisplayNode> {
	
	var onPush: ((UIViewController) -> Void)?
	
	private var mainNode: AnimalCollectionNode
	private var viewModel: AnimalCollectionViewModel
	
	init(viewModel: AnimalCollectionViewModel) {
		let mainNode = AnimalCollectionNode(viewModel: viewModel)
		self.mainNode = mainNode
		self.viewModel = viewModel
		
		super.init(node: mainNode)
		
		configureMainNode()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureNavigationBar()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		mainNode.gradient(from: .lightBlue, to: .darkBlue)
	}
	
	private func configureNavigationBar() {
		
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationController?.navigationBar.shadowImage = UIImage()
		
		let titleLabel = UILabel()
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: 20)
		titleLabel.attributedText = titleStyle.getAttributedString(from: "Animal Collection")
		
		navigationItem.titleView = titleLabel
		
		let backButton = UIButton(type: .custom)
		backButton.setImage(UIImage(named: "icon_back_white"), for: .normal)
		backButton.addTarget(self, action: #selector(popViewController), for: .touchUpInside)
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
	}
	
	private func configureMainNode() {
		mainNode.onOpenDetail = { [weak self] viewModel in
			let viewController = AnimalCollectionDetailViewController(viewModel: viewModel)
			self?.onPush?(viewController)
		}
	}
	
	@objc
	private func popViewController() {
		navigationController?.popViewController(animated: true)
	}
}
