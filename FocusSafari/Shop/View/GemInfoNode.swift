//
//  GemInfoNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 09/01/24.
//

import Foundation
import AsyncDisplayKit

final class GemInfoNode: ASDisplayNode {
	
	private let iconNode: ASImageNode = ASImageNode()
	private let amountNode: ASTextNode = ASTextNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var gemAmount: Int {
		return UserService.shared.userInfo.value?.gem ?? 0
	}
	
	override init() {
		super.init()
		automaticallyManagesSubnodes = true
		
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let stack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 0,
			justifyContent: .spaceBetween,
			alignItems: .center,
			children: [
				iconNode,
				amountNode
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8,
													  left: 8,
													  bottom: 8,
													  right: 8),
								 child: stack)
	}
	
	private func configureNodes() {
		configureImageNode()
		configureTextNode()
	}
	
	private func configureImageNode() {
		iconNode.image = UIImage(named: "icon_shop_gem")
		iconNode.style.preferredSize = CGSize(width: 24, height: isSmallDevice ? 16 : 20)
		iconNode.contentMode = .scaleAspectFit
	}
	
	private func configureTextNode() {
		let amountStyle = FontStyle(font: .medium, color: .deepTeal, size: isSmallDevice ?  14 : 16, alignment: .right)
		amountNode.attributedText = amountStyle.getAttributedString(from: gemAmount.stringWithCommaSeparator)
	}
}
