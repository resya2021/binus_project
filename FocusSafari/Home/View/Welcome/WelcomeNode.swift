//
//  WelcomeNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 11/01/24.
//

import Foundation
import AsyncDisplayKit

final class WelcomeNode: ASDisplayNode {
	
	var onDone: (() -> Void)?
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var inset: CGFloat {
		return isSmallDevice ? 20 : 30
	}
	
	private let titleNode = ASTextNode()
	private let subtitleNode = ASTextNode()
	private let captionNode = ASTextNode()
	private let buttonNode = ASButtonNode()
	
	private lazy var collectionNode: ASCollectionNode = {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .vertical
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		
		let collectionNode = ASCollectionNode(collectionViewLayout: layout)
		
		return collectionNode
	}()
	
	override init() {
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .white
		configureNodes()
	}
	
	override func subnodeDisplayWillStart(_ subnode: ASDisplayNode) {
		buttonNode.gradient(from: .lightBlue, to: .darkBlue)
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let titleStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 0,
			justifyContent: .start,
			alignItems: .start,
			children: [
				titleNode,
				subtitleNode
			])
		
		let collectionStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 0,
			justifyContent: .start,
			alignItems: .start,
			children: [
				captionNode,
				collectionNode
			])
		
		let mainStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 0,
			justifyContent: .spaceBetween,
			alignItems: .start,
			children: [
				titleStack,
				collectionStack,
				buttonNode
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: ScreenInfo.safeAreaInsets.top,
													  left: inset,
													  bottom: ScreenInfo.safeAreaInsets.bottom + 20,
													  right: inset),
								 child: mainStack)
	}
	
	private func configureNodes() {
		configureTextNodes()
		configureButtonNode()
		configureCollectionNode()
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .bold, color: .deepTeal, size: isSmallDevice ?  36 : 40, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: "Welcome to")
		
		let subtitleStyle = FontStyle(font: .bold, color: .teal, size: isSmallDevice ?  40 : 48, alignment: .left)
		subtitleNode.attributedText = subtitleStyle.getAttributedString(from: "Focus Safari!")
		
		let captionStyle = FontStyle(font: .regular, color: .gray61, size: isSmallDevice ?  14 : 16, alignment: .left)
		captionNode.attributedText = captionStyle.getAttributedString(from: "Here is what you’ll get:")
	}
	
	private func configureCollectionNode() {
		collectionNode.dataSource = self
		collectionNode.delegate = self
		collectionNode.backgroundColor = .clear
		collectionNode.view.contentInsetAdjustmentBehavior = .never
		collectionNode.style.preferredSize = CGSize(width: ScreenInfo.width - 2*inset, height: isSmallDevice ? 350 : 450)
	}
	
	private func configureButtonNode() {
		let buttonStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  16 : 20, alignment: .center)
		buttonNode.setAttributedTitle(buttonStyle.getAttributedString(from: "Get Started!"), for: .normal)
		buttonNode.style.preferredSize = CGSize(width: ScreenInfo.width - 2*inset, height: isSmallDevice ?  48 : 60)
		buttonNode.cornerRadius = 10
		buttonNode.addTarget(self, action: #selector(buttonPressed), forControlEvents: .touchUpInside)
	}
	
	@objc
	private func buttonPressed() {
		onDone?()
	}
}

extension WelcomeNode: ASCollectionDataSource, ASCollectionDelegate {
	func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
		return 1
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
		return 3
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
		return {
			switch indexPath.row {
			case 0:
				return WelcomeCellNode(title: "Boost Your Productivity! 🚀",
									   desc: "Choose your focus time, from 15 to 60 minutes, and watch your efficiency rocket with our Pomodoro-inspired method.")
			case 1:
				return WelcomeCellNode(title: "Earn Rewards While You Work! 💎",
									   desc: "Stay motivated as every focused moment rewards you with Exp and Gems, a testament to your hard work and commitment.")
			case 2:
				return WelcomeCellNode(title: "Safari Discovery and Learning! 🏆",
									   desc: "More than just a productivity tool, you'll uncover interesting facts and insights about Wildlife. It’s productivity with a purpose!")
			default:
				return ASCellNode()
			}
		}
	}
}

