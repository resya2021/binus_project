//
//  ShopItemModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation

enum EggRarity: Int, CaseIterable {
	case common
	case rare
	case mythical
	
	var string: String {
		switch self {
		case .mythical:
			return "Mythical"
		case .rare:
			return "Rare"
		case .common:
			return "Common"
		}
	}
}

struct ShopItemModel {
	
	var rarity: EggRarity
	var name: String
	var description: String
	var price: Int
	var assetName: String
	
	init(rarity: EggRarity) {
		self.rarity = rarity
		
		switch rarity {
		case .common:
			self.name = "Common Egg"
			self.description = "Offering a rare chance at Mythical animals and a small probability for Rare ones, it's ideal for those who love surprises."
			self.price = 150
			self.assetName = "egg_common"
		case .rare:
			self.name = "Rare Egg"
			self.description = "With a small chance for Mythical and an high chance for Rare animals, it strikes the perfect balance between hope and possibility."
			self.price = 1500
			self.assetName = "egg_rare"
		case .mythical:
			self.name = "Mythical Egg"
			self.description = "High chance of revealing a Mythical animal, it's the ultimate choice for seekers of the extraordinary."
			self.price = 15000
			self.assetName = "egg_mythical"
		}
	}
}

