//
//  Int.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation

extension Int {
	var stringWithCommaSeparator: String {
		let formatter = NumberFormatter()
		formatter.numberStyle = .decimal
		formatter.groupingSeparator = ","
		formatter.locale = Locale(identifier: "en_US")

		return formatter.string(from: NSNumber(value: self)) ?? ""
	}
}
