//
//  WelcomeViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 11/01/24.
//

import Foundation
import AsyncDisplayKit

final class WelcomeViewController: ASDKViewController<ASDisplayNode> {
	
	private var mainNode: WelcomeNode
	
	override init() {
		self.mainNode = WelcomeNode()
		super.init(node: mainNode)
		
		configureMainNode()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func configureMainNode() {
		mainNode.onDone = { [weak self] in
			self?.dismiss(animated: true)
		}
	}
}
