//
//  AnimalCollectionDetailCellViewModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 12/01/24.
//

import Foundation

final class AnimalCollectionDetailCellViewModel {
	
	let title: String
	let desc: String?
	let multiDesc: [String: String]?
	
	init(title: String, desc: String? = nil, multiDesc: [String: String]? = nil) {
		self.title = title
		self.desc = desc
		self.multiDesc = multiDesc
	}
}
