//
//  AccountNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 10/01/24.
//

import Foundation
import AsyncDisplayKit

enum SettingsNavigation: String {
	case referral
	case shareProfile
	case notificationPref
	case help
	case review
}

final class AccountNode: ASDisplayNode {
	
	var onEditAccount: (() -> Void)?
	var onEditBio: (() -> Void)?
	var onSettingsNavigate: ((SettingsNavigation) -> Void)?
	var onOpenCollection: (() -> Void)?
	var onOpenShop: (() -> Void)?
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private let viewModel: AccountViewModel
	
	private lazy var collectionNode: ASCollectionNode = {
		let layout = UICollectionViewFlowLayout()
		layout.scrollDirection = .vertical
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		
		let collectionNode = ASCollectionNode(collectionViewLayout: layout)
		
		return collectionNode
	}()
	
	init(viewModel: AccountViewModel) {
		self.viewModel = viewModel
		super.init()
		automaticallyManagesSubnodes = true
		
		configureCollectionNode()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: ScreenInfo.safeAreaInsets.top + 30 + 24,
													  left: isSmallDevice ? 16 : 20,
													  bottom: ScreenInfo.safeAreaInsets.bottom,
													  right: isSmallDevice ? 16 : 20),
								 child: collectionNode)
	}
	
	private func configureCollectionNode() {
		collectionNode.dataSource = self
		collectionNode.delegate = self
		collectionNode.backgroundColor = .clear
		collectionNode.view.contentInsetAdjustmentBehavior = .never
	}
	
	func reload(section: Int) {
		collectionNode.reloadSections(IndexSet([section]))
	}
}

extension AccountNode: ASCollectionDataSource, ASCollectionDelegate {
	func numberOfSections(in collectionNode: ASCollectionNode) -> Int {
		return 3
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
		switch section {
		case 2:
			return 5 + 1 // last cell: empty
		default:
			return 1
		}
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
		return { [weak self] in
			guard let self else {
				return ASCellNode()
			}
			
			switch indexPath.section {
			case 0:
				return self.getUserCellNode()
			case 1:
				return self.getBioCellNode()
			case 2:
				return self.getSettingsCellNode(for: indexPath.row)
			default:
				return ASCellNode()
			}
		}
	}
	
	private func getUserCellNode() -> ASCellNode {
		let cell = UserCellNode(viewModel: viewModel)
		
		cell.onEdit = { [weak self] in
			self?.onEditAccount?()
		}
		
		cell.onOpenCollection = { [weak self] in
			self?.onOpenCollection?()
		}
		
		cell.onOpenShop = { [weak self] in
			self?.onOpenShop?()
		}
		
		return cell
	}
	
	private func getBioCellNode() -> ASCellNode {
		let cell = BioCellNode(viewModel: viewModel)
		
		cell.onEdit = { [weak self] in
			self?.onEditBio?()
		}
		
		return cell
	}
	
	private func getSettingsCellNode(for row: Int) -> ASCellNode {
		
		var buttonImageName: String? = nil
		var buttonTitle: String = ""
		var buttonAction: SettingsNavigation = .referral
		
		switch row {
		case 0:
			buttonImageName = "icon_account_refer"
			buttonTitle = "Refer and Get 1 Rare Egg!"
			buttonAction = .referral
		case 1:
			buttonImageName = "icon_account_share"
			buttonTitle = "Share your Ranger Profile"
			buttonAction = .shareProfile
		case 2:
			buttonImageName = "icon_account_notification"
			buttonTitle = "Notification Preference"
			buttonAction = .notificationPref
		case 3:
			buttonImageName = "icon_account_help"
			buttonTitle = "Need Help?"
			buttonAction = .help
		case 4:
			buttonImageName = "icon_account_review"
			buttonTitle = "Give us a review"
			buttonAction = .review
		default:
			buttonImageName = nil
			buttonTitle = ""
		}
		
		return SettingsCellNode(buttonIcon: buttonImageName, buttonTitle: buttonTitle, buttonAction: buttonAction)
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
		guard indexPath.section == 2,
			  indexPath.row < 5,
			  let cell = collectionNode.nodeForItem(at: indexPath) as? SettingsCellNode else {
			return
		}
		
		onSettingsNavigate?(cell.buttonAction)
	}
	
	func collectionNode(_ collectionNode: ASCollectionNode, willDisplayItemWith node: ASCellNode) {
		guard let cellNode = node as? UserCellNode else {
			return
		}
		
		cellNode.setGradient()
	}
}

