//
//  UserService.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation
import RxRelay

final class UserService {
	static let shared: UserService = UserService()
	
	let userInfo: BehaviorRelay<UserModel?> = BehaviorRelay<UserModel?>(value: nil)
	private let userDefaults = UserDefaults.standard
	
	var currentLevelExpRequirement: Int = 1500
	
	var isFirstTime: Bool {
		return ((UserDefaults.standard.object(forKey: UserDefaultsKey.isFirstOpen.rawValue) as? Bool) ?? true)
	}
	
	private init() {
		getSavedUserInfo()
	}
	
	func updateUserInfo(with value: Any, for key: UserDefaultsKey) {
		if key == .userExp && canConvertExpToLevel() {
			convertExpToLevel()
		} else {
			userDefaults.set(value, forKey: key.rawValue)
			
			guard let info = self.userInfo.value else {
				return
			}
			
			info.updateInfo(with: value, for: key)
			userInfo.accept(info)
		}
	}
	
	func updateUserInfo(with dict: [UserDefaultsKey: Any]) {
		guard let info = self.userInfo.value else {
			return
		}
		
		dict.forEach { key, value in
			
			if key == .userExp && canConvertExpToLevel() {
				convertExpToLevel()
			} else {
				userDefaults.set(value, forKey: key.rawValue)
				info.updateInfo(with: value, for: key)
			}
		}
		
		userInfo.accept(info)
	}
	
	private func getSavedUserInfo() {
		let userInfo = UserModel(
			name: ((userDefaults.object(forKey: UserDefaultsKey.userName.rawValue) as? String) ?? "Guest"),
			bio: ((userDefaults.object(forKey: UserDefaultsKey.userName.rawValue) as? String) ?? "Leaping over logs, whispering to wildlife, ensuring every safari trip is packed with fun, education, and excitement!"),
			gem: ((userDefaults.object(forKey: UserDefaultsKey.userGem.rawValue) as? Int) ?? 0),
			egg: ((userDefaults.object(forKey: UserDefaultsKey.userEgg.rawValue) as? Int) ?? 0),
			level: ((userDefaults.object(forKey: UserDefaultsKey.userLevel.rawValue) as? Int) ?? 1),
			exp: ((userDefaults.object(forKey: UserDefaultsKey.userExp.rawValue) as? Int) ?? 0),
			ownedAnimals: ((userDefaults.object(forKey: UserDefaultsKey.ownedAnimalsIds.rawValue) as? [String]) ?? [])
		)
		
		self.userInfo.accept(userInfo)
	}
	
	private func calculateExpRequirement() {
		guard let info = self.userInfo.value else {
			return
		}
		
		let baseExpRequirement = 1500.0
		let expRequirementMultiplier = pow(1.2, Double(info.level-1))
		
		currentLevelExpRequirement = Int(baseExpRequirement * expRequirementMultiplier)
	}
	
	private func convertExpToLevel() {
		guard let info = self.userInfo.value,
			  info.exp >= currentLevelExpRequirement else {
			return
		}
		
		info.level += 1
		info.exp -= currentLevelExpRequirement
		
		userDefaults.set(info.level, forKey: UserDefaultsKey.userLevel.rawValue)
		userDefaults.set(info.exp, forKey: UserDefaultsKey.userExp.rawValue)
		
		userInfo.accept(info)
		calculateExpRequirement()
	}
	
	private func canConvertExpToLevel() -> Bool {
		guard let info = self.userInfo.value,
			  info.exp >= currentLevelExpRequirement else {
			return false
		}
		
		return true
	}
}
