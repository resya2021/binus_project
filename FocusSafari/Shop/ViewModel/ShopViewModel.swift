//
//  ShopViewModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 04/01/24.
//

import Foundation

final class ShopViewModel {
	
	private var gem: Int? {
		return UserService.shared.userInfo.value?.gem
	}
	
	func isGemEnough(for price: Int) -> Bool {
		guard let ownedGem = self.gem else {
			return false
		}
		return price <= ownedGem
	}
	
	func deductGem(for price: Int) {
		guard let ownedGem = self.gem else {
			return
		}
		
		let newGemAmount = ownedGem - price
		UserService.shared.updateUserInfo(with: newGemAmount, for: .userGem)
	}
}
