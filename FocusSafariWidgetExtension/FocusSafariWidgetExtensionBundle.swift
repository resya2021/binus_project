//
//  FocusSafariWidgetExtensionBundle.swift
//  FocusSafariWidgetExtension
//
//  Created by Anisa Budiarthati on 01/01/24.
//

import WidgetKit
import SwiftUI

@main
struct FocusSafariWidgetExtensionBundle: WidgetBundle {
    var body: some Widget {
		if #available(iOSApplicationExtension 16.2, *) {
			FocusSafariWidgetExtensionLiveActivity()
		}
    }
}
