//
//  RewardViewModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 09/01/24.
//

import Foundation

final class RewardViewModel {
	
	let duration: Int
	private let isInterrupted: Bool
	private var rewardExp: Int?
	private var rewardGem: Int?
	
	private var userService: UserService {
		return UserService.shared
	}
	
	init(duration: Int, isInterrupted: Bool) {
		self.duration = duration
		self.isInterrupted = isInterrupted
		
		calculateReward { [weak self] in
			self?.updateUserInfo()
		}
	}
	
	private func calculateReward(completion: (() -> Void)?) {
		// MOCK: for easier UAT
		let tempExpMultiplier: Int = 500
		let tempGemMultiplier: Int = 1000
		
		let expMultiplier: Int = isInterrupted ? 25 : 100
		let gemMultiplier: Int = isInterrupted ? 0 : 1
		
		rewardExp = duration * tempExpMultiplier
		rewardGem = duration * tempGemMultiplier
		
		completion?()
	}
	
	private func updateUserInfo() {
		guard let rewardExp,
			  let rewardGem,
			  let userInfo = userService.userInfo.value else {
			return
		}
		
		let newExp = userInfo.exp + rewardExp
		let newGem = userInfo.gem + rewardGem
		
		userService.updateUserInfo(with: [.userExp: newExp,
										  .userGem: newGem])
	}
}
