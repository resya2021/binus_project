//
//  CreatureModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import Foundation

struct CreatureModel {
	
	var id: String
	var name: String
	var rarity: Int
	var assetName: String
	
	init(id: String, name: String, rarity: Int, assetName: String) {
		self.id = id
		self.name = name
		self.rarity = rarity
		self.assetName = assetName
	}
}

