//
//  LiveActivityView.swift
//  FocusSafariWidgetExtensionExtension
//
//  Created by Anisa Budiarthati on 02/01/24.
//

import Foundation
import SwiftUI
import ActivityKit

@available(iOS 16.2, *)
struct LiveActivityView: View {
	let iconName: String
	let endTime: Date?
	let message: String?

	var body: some View {
		VStack {
			Spacer(minLength: 15)
			HStack(alignment: .center) {
				Spacer(minLength: 15)
				Image(iconName)
					.resizable()
					.aspectRatio(contentMode: .fit)
				Spacer(minLength: 50)
				if let endTime = endTime {
					Text(timerInterval: .now...endTime)
						.font(.largeTitle)
				}
				if let message = message {
					Text(message)
				}
				Spacer(minLength: 15)
			}
			Spacer(minLength: 15)
		}
		.activityBackgroundTint(Color.black.opacity(0.25))
		.activitySystemActionForegroundColor(Color.white)
	}
}
