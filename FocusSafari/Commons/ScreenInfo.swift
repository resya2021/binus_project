//
//  ScreenInfo.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 25/12/23.
//

import Foundation
import UIKit

final class ScreenInfo {
	
	enum Size {
		case small
		case medium
		case large
	}
	
	static var width: CGFloat {
		return UIScreen.main.bounds.size.width
	}
	
	static var height: CGFloat {
		return UIScreen.main.bounds.size.height
	}
	
	static var safeAreaInsets: UIEdgeInsets {
		guard let window: UIWindow = UIApplication.shared.windows.first,
			  UIWindow.instancesRespond(to: #selector(getter: window.safeAreaInsets)) else {
			return .zero
		}
		
		return window.safeAreaInsets
	}
	
	static var size: Size {
		let screen = UIScreen.main.bounds
		let width = screen.size.width
		let height = screen.size.height
		
		if height <= 667 || width <= 375 {
			return Size.small
		} else if height <= 812 {
			return Size.medium
		} else {
			return Size.large
		}
	}
}
