//
//  FeatureCollectionNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 11/01/24.
//

import Foundation
import AsyncDisplayKit

final class FeatureCollectionNode: ASCellNode {
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private let titleNode = ASTextNode()
	
	override init() {
		super.init()
		automaticallyManagesSubnodes = true
		
		configureNodes()
	}
	
	override func didLoad() {
		super.didLoad()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 8,
			justifyContent: .start,
			alignItems: .start,
			children: [
				titleNode,
				getCellNode(for: 0),
				getCellNode(for: 1),
				getCellNode(for: 2),
				getCellNode(for: 3)
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets.zero, child: stack)
	}
	
	private func getCellNode(for row: Int) -> ASDisplayNode {
		switch row {
		case 0:
			return FeatureCellNode(title: "Gamified Focus Session: ",
								   desc: "Stay engaged with our interactive focus sessions. Set your sessions, and start focusing!")
		case 1:
			return FeatureCellNode(title: "Reward System: ",
								   desc: "Earn in-app achievements for your concentration efforts.")
		case 2:
			return FeatureCellNode(title: "Virtual Animal Collection: ",
								   desc: "Use your achievements to participate in our exclusive Gacha system, collecting and learning about various virtual animals.")
		case 3:
			return FeatureCellNode(title: "Educational Twist: ",
								   desc: "Each collected animal comes with fascinating facts, promoting wildlife awareness and conservation.")
		default:
			return ASDisplayNode()
		}
	}
	
	private func configureNodes() {
		configureTextNode()
	}
	
	private func configureTextNode() {
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: isSmallDevice ?  16 : 18, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: "Main Features 🔑")
	}
}
