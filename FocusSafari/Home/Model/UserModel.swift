//
//  UserModel.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 24/12/23.
//

import Foundation

class UserModel {
	
	var name: String
	var bio: String
	var gem: Int
	var egg: Int
	var level: Int
	var exp: Int
	var ownedAnimals: [String]
	
	init(name: String, bio: String, gem: Int, egg: Int, level: Int, exp: Int, ownedAnimals: [String]) {
		self.name = name
		self.bio = bio
		self.gem = gem
		self.egg = egg
		self.level = level
		self.exp = exp
		self.ownedAnimals = ownedAnimals
	}
	
	init(dict: [UserDefaultsKey: Any]) {
		self.name = ((dict[UserDefaultsKey.userName] as? String) ?? "")
		self.bio = ((dict[UserDefaultsKey.userBio] as? String) ?? "")
		self.gem = ((dict[UserDefaultsKey.userGem] as? Int) ?? 0)
		self.egg = ((dict[UserDefaultsKey.userEgg] as? Int) ?? 0)
		self.level = ((dict[UserDefaultsKey.userLevel] as? Int) ?? 1)
		self.exp = ((dict[UserDefaultsKey.userExp] as? Int) ?? 0)
		self.ownedAnimals = ((dict[UserDefaultsKey.ownedAnimalsIds] as? [String]) ?? [])
	}
	
	func updateInfo(with value: Any, for key: UserDefaultsKey) {
		switch key {
		case .userName:
			self.name = ((value as? String) ?? "")
		case .userBio:
			self.bio = ((value as? String) ?? "")
		case .userGem:
			self.gem = ((value as? Int) ?? 0)
		case .userEgg:
			self.egg = ((value as? Int) ?? 0)
		case .userLevel:
			self.level = ((value as? Int) ?? 1)
		case .userExp:
			self.exp = ((value as? Int) ?? 0)
		case .ownedAnimalsIds:
			self.ownedAnimals = ((value as? [String]) ?? [])
		default:
			break
		}
	}
}

