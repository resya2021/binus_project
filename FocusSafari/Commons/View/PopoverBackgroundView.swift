//
//  PopoverBackgroundView.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 21/01/24.
//

import UIKit

final class PopoverBackgroundView: UIPopoverBackgroundView {
	
	private var offset = CGFloat(0)
	private var arrow = UIPopoverArrowDirection.any
	
	override var arrowDirection: UIPopoverArrowDirection {
		get { return arrow }
		set { arrow = newValue }
	}
	
	override var arrowOffset: CGFloat {
		get { return offset }
		set { offset = newValue }
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setUpView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setUpView()
	}
	
	override static func arrowBase() -> CGFloat {
		return 0
	}
	
	override static func contentViewInsets() -> UIEdgeInsets {
		return .zero
	}
	
	override static func arrowHeight() -> CGFloat {
		return 0
	}
	
	// MARK: - Private
	
	private func setUpView() {
		layer.backgroundColor = UIColor.darkNavy.cgColor
		layer.shadowColor = UIColor.darkNavy.cgColor
	}
}
