//
//  Constants.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 01/01/24.
//

import Foundation

enum Constants {
	static let animalsAPI = "https://api.api-ninjas.com/v1/animals?name="
}

enum UserDefaultsKey: String {
	case userName = "userName"
	case userBio = "userBio"
	case userGem = "userGem"
	case userEgg = "userEgg"
	case userLevel = "userLevel"
	case userExp = "userExp"
	case ownedAnimalsIds = "ownedAnimalsIds"
	
	case isFirstOpen = "isFirstOpen"
}
