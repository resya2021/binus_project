//
//  UserCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 10/01/24.
//

import Foundation
import AsyncDisplayKit
import RxRelay

final class UserCellNode: ASCellNode {
	
	var onEdit: (() -> Void)?
	var onOpenCollection: (() -> Void)?
	var onOpenShop: (() -> Void)?
	
	private let avatarNode = ASImageNode()
	private let nameNode = ASTextNode()
	
	private let editButtonNode = ASButtonNode()
	
	private let rankNode = ASTextNode()
	private let rankIconNode = ASImageNode()
	private let rankLevelNode = ASTextNode()
	
	private let animalButtonNode: ButtonNode
	private let animalCaptionNode = ASTextNode()
	
	private let gemButtonNode: ButtonNode
	private let gemCaptionNode = ASTextNode()
	
	private let expBarOutline = ASDisplayNode()
	private let expBarFill = ASDisplayNode()
	private let expCaptionLeft = ASTextNode()
	private let expCaptionRight = ASTextNode()
	
	private var isSmallDevice: Bool {
		return ScreenInfo.size == .small
	}
	
	private var cellInset: CGFloat {
		return isSmallDevice ?  16 : 20
	}
	
	private var stackSpacing: CGFloat {
		return isSmallDevice ?  10 : 12
	}
	
	private var contentWidth: CGFloat {
		return ScreenInfo.width - cellInset*2 - stackSpacing*2
	}
	
	private let viewModel: AccountViewModel
	
	init(viewModel: AccountViewModel) {
		self.viewModel = viewModel
		self.animalButtonNode = ButtonNode(iconName: "icon_account_animal",
										   text: "\(UserService.shared.userInfo.value?.ownedAnimals.count ?? 0)")
		self.gemButtonNode = ButtonNode(iconName: "icon_account_gem",
										text: "\(UserService.shared.userInfo.value?.gem ?? 0)")
		super.init()
		automaticallyManagesSubnodes = true
		cornerRadius = 10
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let baselineStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 0,
			justifyContent: .spaceBetween,
			alignItems: .start,
			children: [
				nameNode,
				editButtonNode
			])
		
		let contentStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: isSmallDevice ?  6 : 8,
			justifyContent: .start,
			alignItems: .stretch,
			children: [
				baselineStack,
				getRankStack(),
				getStatStack()
			])
		
		contentStack.style.flexGrow = 1
		
		let mainStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: stackSpacing,
			justifyContent: .center,
			alignItems: .stretch,
			children: [
				avatarNode,
				contentStack
			])
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: stackSpacing,
			justifyContent: .start,
			alignItems: .stretch,
			children: [
				mainStack,
				getExpStack()
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: stackSpacing,
													  left: stackSpacing,
													  bottom: stackSpacing,
													  right: stackSpacing),
								 child: stack)
	}
	
	private func getRankStack() -> ASLayoutElement {
		let levelStack = ASOverlayLayoutSpec(child: rankIconNode, overlay: rankLevelNode)
		
		return ASStackLayoutSpec(
			direction: .horizontal,
			spacing: stackSpacing,
			justifyContent: .start,
			alignItems: .center,
			children: [
				rankNode,
				levelStack
			])
	}
	
	private func getStatStack() -> ASLayoutElement {
		let animalStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 4,
			justifyContent: .start,
			alignItems: .stretch,
			children: [
				animalButtonNode,
				animalCaptionNode
			])
		
		let gemStack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 4,
			justifyContent: .start,
			alignItems: .stretch,
			children: [
				gemButtonNode,
				gemCaptionNode
			])
		
		gemStack.style.flexGrow = 1
		
		return ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 8,
			justifyContent: .start,
			alignItems: .stretch,
			children: [
				animalStack,
				gemStack
			])
	}
	
	private func getExpStack() -> ASLayoutElement {
		let textStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 0,
			justifyContent: .spaceBetween,
			alignItems: .center,
			children: [
				expCaptionLeft,
				expCaptionRight
			])
		
		let insetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2), child: expBarFill)
		let barStack = ASAbsoluteLayoutSpec(children: [expBarOutline, insetSpec])
		
		let stack = ASStackLayoutSpec(
			direction: .vertical,
			spacing: 4,
			justifyContent: .center,
			alignItems: .stretch,
			children: [
				textStack,
				barStack
			])
		
		stack.style.maxSize = CGSize(width: contentWidth, height: 50)
		
		return stack
	}
	
	func setGradient() {
		gradient(from: .white, to: .gradientGray)
		animalButtonNode.setGradient()
		gemButtonNode.setGradient()
		expBarFill.gradient(from: .lightYellow, to: .darkYellow, cornerRadius: 3)
	}
	
	private func configureNodes() {
		configureImageNodes()
		configureTextNodes()
		configureButtonNodes()
		configureExpBarNodes()
	}
	
	private func configureImageNodes() {
		avatarNode.image = UIImage(named: "avatar_default")
		avatarNode.style.preferredSize = CGSize(width: 124, height: 124)
		avatarNode.contentMode = .scaleAspectFit
		
		rankIconNode.image = UIImage(named: "icon_rank")
		rankIconNode.style.preferredSize = CGSize(width: 27, height: 27)
		rankIconNode.contentMode = .scaleAspectFit
	}
	
	private func configureTextNodes() {
		let nameStyle = FontStyle(font: .semiBold, color: .teal, size: isSmallDevice ?  20 : 24, alignment: .left)
		nameNode.attributedText = nameStyle.getAttributedString(from: viewModel.userInfo?.name ?? "")
		
		let rankFontSize: CGFloat = isSmallDevice ?  14 : 16
		
		let rankStyle = FontStyle(font: .medium, color: .deepTeal, size: rankFontSize, alignment: .left)
		rankNode.attributedText = rankStyle.getAttributedString(from: "Ranger Rank")
		
		let rankLevelStyle = FontStyle(font: .medium, color: .white, size: rankFontSize, alignment: .center)
		rankLevelNode.attributedText = rankLevelStyle.getAttributedString(from: "\(viewModel.userInfo?.level ?? 1)")
		rankLevelNode.textContainerInset = UIEdgeInsets(top: (27-rankFontSize)/4, left: 0, bottom: 0, right: 0)
		
		let captionStyle = FontStyle(font: .regular, color: .deepTeal, size: 8, alignment: .left)
		animalCaptionNode.attributedText = captionStyle.getAttributedString(from: "Wildlife Discovered")
		gemCaptionNode.attributedText = captionStyle.getAttributedString(from: "Gems Owned")
		
		let expCaptionStyle = FontStyle(font: .regular, color: .gray61, size: 10, alignment: .left)
		expCaptionLeft.attributedText = expCaptionStyle.getAttributedString(from: "Ranger Experience")
		expCaptionRight.attributedText = expCaptionStyle.getAttributedString(from: "\(viewModel.exp)/\(viewModel.currentLevelExpRequirement)")
	}
	
	private func configureButtonNodes() {
		editButtonNode.setImage(UIImage(named: "icon_edit_blue"), for: .normal)
		editButtonNode.addTarget(self, action: #selector(editProfile), forControlEvents: .touchUpInside)
		editButtonNode.borderWidth = 1
		editButtonNode.borderColor = UIColor.teal.cgColor
		editButtonNode.cornerRadius = 5
		editButtonNode.contentEdgeInsets = UIEdgeInsets(top: 3, left: 8, bottom: 3, right: 8)
		
		let editButtonStyle = FontStyle(font: .regular, color: .teal, size: 12, alignment: .right)
		editButtonNode.setAttributedTitle(editButtonStyle.getAttributedString(from: "Edit"), for: .normal)
		
		animalButtonNode.style.preferredSize = CGSize(width: 80, height: 40)
		animalButtonNode.onDidTap = { [weak self] in
			self?.onOpenCollection?()
		}
		
		gemButtonNode.onDidTap = { [weak self] in
			self?.onOpenShop?()
		}
	}
	
	private func configureExpBarNodes() {
		
		expBarOutline.style.preferredSize = CGSize(width: contentWidth, height: 20)
		expBarOutline.borderWidth = 2
		expBarOutline.borderColor = UIColor.darkBlue.cgColor
		expBarOutline.cornerRadius = 5
		
		let progress: CGFloat = CGFloat(viewModel.exp) / CGFloat(viewModel.currentLevelExpRequirement)
		expBarFill.style.preferredSize = CGSize(width: progress*(contentWidth-4), height: 16)
	}
	
	@objc
	private func editProfile() {
		onEdit?()
	}
}

