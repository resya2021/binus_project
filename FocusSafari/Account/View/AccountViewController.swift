//
//  AccountViewController.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 10/01/24.
//

import Foundation
import AsyncDisplayKit

final class AccountViewController: ASDKViewController<ASDisplayNode> {
	
	var onPush: ((UIViewController) -> Void)?
	
	private var mainNode: AccountNode?
	private var viewModel: AccountViewModel?
	private var isFirstDidLayoutSubviews: Bool = true
	
	init(viewModel: AccountViewModel) {
		let mainNode = AccountNode(viewModel: viewModel)
		self.mainNode = mainNode
		self.viewModel = viewModel
		
		super.init(node: mainNode)
		
		configureMainNode()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureNavigationBar()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		if isFirstDidLayoutSubviews {
			mainNode?.gradient(from: .lightBlue, to: .darkBlue)
			isFirstDidLayoutSubviews = false
		}
	}
	
	private func configureNavigationBar() {
		
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationController?.navigationBar.shadowImage = UIImage()
		
		let titleLabel = UILabel()
		let titleStyle = FontStyle(font: .semiBold, color: .white, size: 20)
		titleLabel.attributedText = titleStyle.getAttributedString(from: "Account")
		
		navigationItem.titleView = titleLabel
		
		let backButton = UIButton(type: .custom)
		backButton.setImage(UIImage(named: "icon_back_white"), for: .normal)
		backButton.addTarget(self, action: #selector(popViewController), for: .touchUpInside)
		navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
	}
	
	private func configureMainNode() {
		mainNode?.onEditAccount = { [weak self] in
			
			let alertController = UIAlertController(title: "Edit Name", message: "", preferredStyle: .alert)
			
			alertController.addTextField { textField in
				textField.text = UserService.shared.userInfo.value?.name
			}
			
			let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
				guard let newUserName = alertController.textFields?.first?.text else {
					return
				}
				UserService.shared.updateUserInfo(with: newUserName, for: .userName)
				self?.mainNode?.reload(section: 0)
			}
			
			alertController.addAction(confirmAction)
			self?.present(alertController, animated: true, completion:{})
		}
		
		mainNode?.onEditBio = { [weak self] in
			
			let alertController = UIAlertController(title: "Edit Bio", message: "", preferredStyle: .alert)
			
			alertController.addTextField { textField in
				textField.text = UserService.shared.userInfo.value?.bio
			}
			
			let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
				guard let newBio = alertController.textFields?.first?.text else {
					return
				}
				UserService.shared.updateUserInfo(with: newBio, for: .userBio)
				self?.mainNode?.reload(section: 1)
			}
			
			alertController.addAction(confirmAction)
			self?.present(alertController, animated: true, completion:{})
		}
		
		mainNode?.onSettingsNavigate = { [weak self] navigation in
			
			let alertController = UIAlertController(title: "Settings", message: "\(navigation.rawValue.capitalized)\nComing soon!", preferredStyle: UIAlertController.Style.alert)
			let confirmAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { _ in
				//
			}
			
			alertController.addAction(confirmAction)
			self?.present(alertController, animated: true, completion:{})
		}
		
		mainNode?.onOpenCollection = { [weak self] in
			let vc = AnimalCollectionViewController(viewModel: AnimalCollectionViewModel())
			self?.onPush?(vc)

		}
		
		mainNode?.onOpenShop = { [weak self] in
			let vc = ShopViewController(viewModel: ShopViewModel())
			self?.onPush?(vc)
		}
	}
	
	@objc
	private func popViewController() {
		navigationController?.popViewController(animated: true)
	}
}
