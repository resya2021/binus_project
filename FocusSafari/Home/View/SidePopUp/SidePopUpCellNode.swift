//
//  SidePopUpCellNode.swift
//  FocusSafari
//
//  Created by Anisa Budiarthati on 05/01/24.
//

import Foundation
import AsyncDisplayKit

final class SidePopUpCellNode: ASCellNode {
	
	var buttonAction: NavigationEvent
	
	private let imageNode = ASImageNode()
	private let titleNode = ASTextNode()
	
	private let buttonIcon: String
	private let buttonTitle: String
	
	init(buttonIcon: String, buttonTitle: String, buttonAction: NavigationEvent) {
		self.buttonIcon = buttonIcon
		self.buttonTitle = buttonTitle
		self.buttonAction = buttonAction
		
		super.init()
		automaticallyManagesSubnodes = true
		backgroundColor = .clear
		configureNodes()
	}
	
	override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
		
		let mainStack = ASStackLayoutSpec(
			direction: .horizontal,
			spacing: 10,
			justifyContent: .start,
			alignItems: .center,
			children: [
				imageNode,
				titleNode
			])
		
		return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10,
													  left: 20,
													  bottom: 10,
													  right: 15),
								 child: mainStack)
	}
	
	private func configureNodes() {
		configureImageNodes()
		configureTextNodes()
	}
	
	private func configureImageNodes() {
		imageNode.image = UIImage(named: buttonIcon)
		imageNode.style.preferredSize = CGSize(width: 30, height: 30)
		imageNode.contentMode = .scaleAspectFit
	}
	
	private func configureTextNodes() {
		let titleStyle = FontStyle(font: .medium, color: .deepTeal, size: 16, alignment: .left)
		titleNode.attributedText = titleStyle.getAttributedString(from: buttonTitle)
	}
}
